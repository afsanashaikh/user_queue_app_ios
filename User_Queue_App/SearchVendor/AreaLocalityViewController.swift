//
//  AreaLocalityViewController.swift
//  User_Queue_App
//
//  Created by Mac on 16/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation


class AreaLocalityViewController: UIViewController,UITableViewDataSource ,UITableViewDelegate ,GMSAutocompleteViewControllerDelegate,CLLocationManagerDelegate,CAAnimationDelegate {
   
    
    @IBOutlet weak var lblAreaLocality: UILabel!
    
    
    @IBOutlet weak var lblUseMyCurrentLocation: UILabel!
    
    
    @IBOutlet weak var lblRecentlySearchedArea: UILabel!
    
    
    
    
    @IBOutlet weak var txtFieldSerachArea: UITextField!
    
    var arrlist = NSMutableArray()
    var locationManager = CLLocationManager()
    var areaLocalityFlag = false
    
    
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        print("Place attributions: \(place.attributions)")
        print(place.coordinate)
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        print(place)
        
        var str: NSString!
        
        str = place.name as! NSString
        
        txtFieldSerachArea.text = str as! String
        
        UserDefaults.standard.set(place.coordinate.latitude, forKey: "lat")
        UserDefaults.standard.set(place.coordinate.longitude, forKey: "long")
        UserDefaults.standard.set(place.name, forKey: "place")
        
        print(UserDefaults.standard.value(forKey: "lat"))
        print(UserDefaults.standard.value(forKey: "long"))
        print(UserDefaults.standard.value(forKey: "place"))
        
        dismiss(animated: true, completion: nil)
        
        
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:SearchVendorViewController =
            storyboard?.instantiateViewController(withIdentifier: "SearchVendorViewController") as!
        SearchVendorViewController
        objVc.areaFlag = true
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    

    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var tableViewSearch: UITableView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        searchView.layer.cornerRadius = 10
        
        get_Recently_Searched_Area()
        
        
       
        
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        print(locValue)
        
        latLong(lat: locValue.latitude, long: locValue.longitude)
        
      //  let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:SearchVendorViewController =
            storyboard?.instantiateViewController(withIdentifier: "SearchVendorViewController") as!
        SearchVendorViewController
        objVc.areaFlag = true
        self.navigationController?.isNavigationBarHidden = true
        
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
   
    func latLong(lat: Double,long: Double)  {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat , longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            print("Response GeoLocation : \(placemarks)")
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? String {
                print("Country :- \(country)")
                // City
                if let city = placeMark.addressDictionary!["City"] as? String {
                    print("City :- \(city)")
                    
                    UserDefaults.standard.set(lat, forKey: "lat")
                    UserDefaults.standard.set(long, forKey: "long")
                    
                    UserDefaults.standard.set(city, forKey: "place")
                
                    // State
                    if let state = placeMark.addressDictionary!["State"] as? String{
                        print("State :- \(state)")
                        // Street
                        if let street = placeMark.addressDictionary!["Street"] as? String{
                            print("Street :- \(street)")
                            let str = street
                            let streetNumber = str.components(
                                separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                            print("streetNumber :- \(streetNumber)" as Any)
                            
                            // ZIP
                            if let zip = placeMark.addressDictionary!["ZIP"] as? String{
                                print("ZIP :- \(zip)")
                                // Location name
                                if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                                    print("Location Name :- \(locationName)")
                                    // Street address
                                    if let thoroughfare = placeMark?.addressDictionary!["Thoroughfare"] as? NSString {
                                        print("Thoroughfare :- \(thoroughfare)")
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    

    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrlist.count    
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableViewSearch.register(UINib(nibName: "RecentlySearcgedAreaCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecentlySearcgedAreaCell
        
        cell1.selectionStyle = .none
        
        let dict = arrlist[indexPath.row] as! NSDictionary
        
        let areaname = dict.value(forKey: "AreaName") as! String

        
        cell1.lblAreaName.text = areaname
        
        
        return cell1
    }
    
    
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath)
    {
        let dict = arrlist[indexPath.row] as! NSDictionary
        
        let areaname = dict.value(forKey: "AreaName") as! String
        
        txtFieldSerachArea.text = areaname
        
        UserDefaults.standard.set(dict.value(forKey: "Latitude"), forKey: "lat")
        UserDefaults.standard.set(dict.value(forKey: "Longitude"), forKey: "long")
        
        UserDefaults.standard.set(areaname, forKey: "place")
        
        print(UserDefaults.standard.value(forKey: "lat"))
        print(UserDefaults.standard.value(forKey: "long"))
        print(UserDefaults.standard.value(forKey: "place"))
        
        
      
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:SearchVendorViewController =
            storyboard?.instantiateViewController(withIdentifier: "SearchVendorViewController") as!
        SearchVendorViewController
        objVc.areaFlag = true
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func btnUseMyCurrentLocationClicked(_ sender: Any)
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
   //Google palces
    
    
    
    // Present the Autocomplete view controller when the button is pressed.
//    @objc func autocompleteClicked(_ sender: UIButton) {
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self as! GMSAutocompleteViewControllerDelegate
//
//        // Specify the place data types to return.
//        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//            UInt(GMSPlaceField.placeID.rawValue))!
//        autocompleteController.placeFields = fields
//
//        // Specify a filter.
//        let filter = GMSAutocompleteFilter()
//        filter.type = .address
//        autocompleteController.autocompleteFilter = filter
//
//        // Display the autocomplete view controller.
//        present(autocompleteController, animated: true, completion: nil)
//    }
    
    
    
    @IBAction func btnSearchAreaClicked(_ sender: Any)
    {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
     //  let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//        UInt(GMSPlaceField.placeID.rawValue))!
      //  autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    
    
    func get_Recently_Searched_Area() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        let userid = UserDefaults.standard.value(forKey: "UserId")
        
        
        Param["UserId"] = userid
        //  Param["Type"] = type
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetSearchedAreaByUser)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetSearchedAreaByUser+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        if let data =  dictionary["Response"] as? NSArray{
                            self.arrlist = data.mutableCopy() as! NSMutableArray
                            
                            print(self.arrlist)
                        }
                    self.tableViewSearch.reloadData()
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
}

/*func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
   
    dismiss(animated: true, completion: nil)
}

func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
}

// User canceled the operation.
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
}

// Turn the network activity indicator on and off again.
func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
}

func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
}*/
