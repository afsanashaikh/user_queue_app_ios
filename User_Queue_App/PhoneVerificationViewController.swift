//
//  PhoneVerificationViewController.swift
//  User_Queue_App
//
//  Created by Mac on 05/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class PhoneVerificationViewController: UIViewController ,UITextFieldDelegate{

    
    
    @IBOutlet weak var btnResendOtp: UIButton!
    @IBOutlet weak var txtfield1: UITextField!
    @IBOutlet weak var txtfield2: UITextField!
    @IBOutlet weak var txtfield3: UITextField!
    @IBOutlet weak var txtField4: UITextField!
    
    @IBOutlet weak var txtField5: UITextField!
    
    @IBOutlet weak var txtField6: UITextField!
    
    @IBOutlet weak var lblOtpHasBeenSent: UILabel!
    
    @IBOutlet weak var lblPhoneVerification: UILabel!
    
    var phoneNumber: String!
    var countryCode : String!
    var otpString: String!
    var finalText: String!
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
      self.navigationController?.popViewController(animated: true)
    }
    
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        
        txtfield1.layer.cornerRadius = 10.0
        txtfield2.layer.cornerRadius = 10.0
        txtfield3.layer.cornerRadius = 10.0
        txtField4.layer.cornerRadius = 10.0
        txtField5.layer.cornerRadius = 10.0
        txtField6.layer.cornerRadius = 10.0

         txtfield1.delegate = self
         txtfield2.delegate = self
         txtfield3.delegate = self
         txtField4.delegate = self
         txtField5.delegate = self
         txtField6.delegate = self
        
        txtfield1.textContentType = .oneTimeCode
        txtfield2.textContentType = .oneTimeCode
        txtfield3.textContentType = .oneTimeCode
        txtField4.textContentType = .oneTimeCode
        txtField5.textContentType = .oneTimeCode
        txtField6.textContentType = .oneTimeCode
        
        
        txtfield1.becomeFirstResponder()
    
        changeLabel()
    }
    func changeLabel() {
        lblPhoneVerification.text = getLabel(langId: "1", labelId: "Phone Verification")
        //print(lblJoinQueue.text as!String)
        
        
        lblOtpHasBeenSent.text = getLabel(langId: "1", labelId: "OTP has been sent to") + " " + countryCode + " " + phoneNumber
        
        btnResendOtp.setTitle(getLabel(langId: "1", labelId: "Resend OTP"), for: .normal)
        
        
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
         finalText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)as String
        
        if textField == txtfield1
        
        {
            txtfield1.text = finalText
            
            if string != ""
            {
                txtfield2.becomeFirstResponder()
            }
            
        }
        
        if textField == txtfield2
            
        {
            txtfield2.text = finalText
            
            if string != ""
            {
                txtfield3.becomeFirstResponder()
            }
            
        }
        
        if textField == txtfield3
            
        {
            txtfield3.text = finalText
            
            if string != ""
            {
                txtField4.becomeFirstResponder()
            }
            
        }
        
        if textField == txtField4
            
        {
            txtField4.text = finalText
            
            if string != ""
            {
                txtField5.becomeFirstResponder()
            }
            
        }
        if textField == txtField5
            
        {
            txtField5.text = finalText
            
            if string != ""
            {
                txtField6.becomeFirstResponder()
            }
            
        }
        if textField == txtField6
            
        {
           // txtField6.text = finalText
            
            if string != ""
            {
//
            //  txtField6.text = finalText
                verifyOtp()
            }
            
        }
        return true
    }
    
    
    func verifyOtp() {
    
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        var myString = self.countryCode as! String
        myString.remove(at: myString.startIndex)
        
        print(myString)
        
        let mobileNumber = myString + phoneNumber
        
        
        let otpStr = self.txtfield1.text! + self.txtfield2.text! + self.txtfield3.text! + self.txtField4.text! + self.txtField5.text! + finalText!
        
        
        
        let otpString = otpStr
        
      //  let mobileNumber = self.phoneNumber
        //  let mobileNumber =  "918149447991"
        
      //  let otpString = self.txtfield1.text + self.txtfield2.text + self.txtfield3.text + self.txtField4.text self.txtField5.text + self.txtField6.text
        //let type = 2
        
        Param["MobileNo"] = mobileNumber
        Param["OTP"] = otpString
      //  Param["Type"] = type
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+verify_Otp)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+verify_Otp+"?" )! , method: .post, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                       let dict = dictionary["Response"]as! NSDictionary
                       let userid = dict["Id"]
                        UserDefaults.standard.set(userid, forKey: "UserId")
                        print(UserDefaults.standard.value(forKey: "UserId"))
//                        
                        if responseStr == "NEW"
                        {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVc=storyBoard .instantiateViewController(withIdentifier: "RegistrationViewController")
                            self.navigationController?.isNavigationBarHidden = true
                            self.navigationController?.pushViewController(objVc, animated: true)
                        }
                       else
                        {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
                            
                            self.navigationController?.isNavigationBarHidden = true
                            self.navigationController?.pushViewController(objVc, animated: true)
                        }
                       
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            self.Toast(Title: "", Text: dictionary["Message"] as! String, delay: 5)

                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    self.Toast(Title: "", Text: dictionary["Message"] as! String, delay: 5)

                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
              //  Toast(Title: "", Text: Dictionary["Message"], delay: 5)

                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    
    @IBAction func btnResendOtpClicked(_ sender: Any) {
        
        
        
        txtfield1.text = ""
        txtfield2.text = ""
        txtfield3.text = ""
        txtField4.text = ""
        txtField5.text = ""
        txtField6.text = ""
        
       call_Get_Otp()
        
    }
    
    func call_Get_Otp()
        
    {
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
       var Param = [String: Any]()

        var myString = self.countryCode as! String
        myString.remove(at: myString.startIndex)
        
        print(myString)
        
        let mobileNumber = myString + phoneNumber
        
        let hashKey = " "
        let type = 2
        
        Param["MobileNo"] = mobileNumber
        Param["HashKey"] = hashKey
        Param["Type"] = type
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+send_Otp)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+send_Otp+"?" )! , method: .post, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Status"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        /* if let dataDict = dictionary["data"] as? [[String:Any]]
                         {
                         self.mealDate = dataDict
                         print("mealDate:\(self.mealDate)")
                         
                         DispatchQueue.main.async {
                         self.collViewHeight.constant = 300
                         self.mealCollView.reloadData()
                         self.mealCollView.layoutIfNeeded()
                         self.collViewHeight.constant = self.mealCollView.contentSize.height
                         }
                         }*/
                        
                        /*  let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                         let objVc=storyBoard .instantiateViewController(withIdentifier: "PhoneVerificationViewController")
                         self.present(objVc, animated: true, completion: nil)*/
                        
//                        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                        let objVc:PhoneVerificationViewController =
//                            stroyboard.instantiateViewController(withIdentifier: "PhoneVerificationViewController") as!
//                        PhoneVerificationViewController
//
//
//
//                        var otpString: String = (dictionary["Message"] as? String)!
//                        var otpStringArr = otpString.components(separatedBy: ".")
//
//                        objVc.otpString = otpStringArr[1]
//
//                        self.present(objVc, animated: true, completion: nil)
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            self.Toast(Title: "", Text: dictionary["Message"] as! String, delay: 5)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    self.Toast(Title: "", Text: dictionary["Message"] as! String, delay: 5)

                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                self.Toast(Title: "", Text: "Server not responding please try again later", delay: 5)

                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
    }
    
    func Toast(Title:String ,Text:String, delay:Int) -> Void {
        let alert = UIAlertController(title: Title, message: Text, preferredStyle: .alert)
        self.present(alert, animated: true)
        let deadlineTime = DispatchTime.now() + .seconds(delay)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
