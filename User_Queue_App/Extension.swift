////
////  Extension.swift
////  User_Queue_App
////
////  Created by Mac on 22/05/20.
////  Copyright © 2020 Mac. All rights reserved.
////
//
//import Foundation
//import SwiftyJSON
//extension UIViewController: NVActivityIndicatorViewable,MFMailComposeViewControllerDelegate {
//
//    func removeChildViewController(){
//        if self.children.count > 0{
//            let viewControllers:[UIViewController] = self.children
//            for viewContoller in viewControllers{
//                viewContoller.willMove(toParent: nil)
//                viewContoller.view.removeFromSuperview()
//                viewContoller.removeFromParent()
//            }
//        }
//    }
//
//    func add(_ child: UIViewController) {
//        addChild(child)
//        view.addSubview(child.view)
//        child.didMove(toParent: self)
//    }
//
//    func remove() {
//        guard parent != nil else {
//            return
//        }
//
//        willMove(toParent: nil)
//        view.removeFromSuperview()
//        removeFromParent()
//    }
//
//    static func loadFromNib() -> Self {
//        func instantiateFromNib<T: UIViewController>() -> T {
//            return T.init(nibName: String(describing: T.self), bundle: nil)
//        }
//        return instantiateFromNib()
//    }
//
//    func convertToDictionary(text: String) -> [[String:Any]] {
//
//        if let data = text.data(using: .utf8) {
//            do {
//                return try JSONSerialization.jsonObject(with: data, options: []) as! [[String : Any]]
//            } catch {
//                print(error.localizedDescription)
//                return []
//            }
//        }
//        return []
//    }
//
//    //MARK:- Send Email
//    func sendEmail(_ subject:String,_ email:String) {
//        if MFMailComposeViewController.canSendMail() {
//            let mail = MFMailComposeViewController()
//            mail.mailComposeDelegate = self
//            mail.setToRecipients([email])
//            mail.setSubject(subject)
//            present(mail, animated: true)
//        } else {
//            // show failure alert
//        }
//    }
//
//    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        controller.dismiss(animated: true)
//    }
//
//    func removeAllDeliverNotification() {
//        UIApplication.shared.applicationIconBadgeNumber = 1
//        UIApplication.shared.applicationIconBadgeNumber = 0
//    }
//
//    // Go Back Action
//    @IBAction func goBack(_ sender: UIControl) {
//        self.navigationController!.popViewController(animated: true)
//    }
//
//    // Return AuthToken
//    func getToken() -> String {
//        let authToken = UserDefaults.standard.object(forKey: "token")
//        return (authToken == nil ? "" : authToken as! String)
//    }
//
//    // Return DeviceToken
//    func getDeviceToken() -> String {
//        let deviceToken = UserDefaults.standard.object(forKey: "DeviceToken")
//        return (deviceToken == nil ? "" : deviceToken as! String)
//    }
//
//    // Showing Toast Message
//    func showTostMessage(message: String, isSuccess:Bool = false, isFromBottom:Bool = true) {
//        self.view.endEditing(true)
//        if message != "" {
//            ToastView.appearance().backgroundColor = isSuccess ? #colorLiteral(red: 0.5882352941, green: 0.7058823529, blue: 0.2666666667, alpha: 1) : #colorLiteral(red: 0.8588235294, green: 0.3137254902, blue: 0.2901960784, alpha: 1)
//            ToastView.appearance().textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            ToastView.appearance().font = UIFont.init(name: "GothamHTF-Medium", size: 14.0)
//            //            ToastView.appearance().bottomOffsetPortrait = isFromBottom ? (appDelegate.tabController == nil ? 30: appDelegate.tabController.tabBar.frame.size.height+8) : screenHeight - (UIApplication.shared.keyWindow?.safeAreaInsets.top)! - 50
//            Toast(text: message).show()
//        }
//    }
//
//    func showConfirmation(title:String = "Catalet", message:String, completion:(() -> Void)?) {
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alertController.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (alert) in
//            guard completion != nil else {
//                return
//            }
//            completion!()
//        }))
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    func randomString() -> String {
//        let len: Int = 3
//        let needle : NSString = "0123456789"
//        let randomString : NSMutableString = NSMutableString(capacity: len)
//
//        for _ in 0..<len {
//            let length = UInt32 (needle.length)
//            let rand = arc4random_uniform(length)
//            randomString.appendFormat("%C", needle.character(at: Int(rand)))
//        }
//        return randomString as String
//    }
//
//    // Show LoadingView When API is called
//    func showLoading(_ color: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) {
//        let size = CGSize(width: 50, height:50)
//        startAnimating(size, message: nil, type: .ballClipRotate, color: color, backgroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0))
//    }
//
//    // Hide LoadingView
//    func hideLoading() {
//        stopAnimating()
//    }
//
//    func hideKeyboard() {
//        DispatchQueue.main.async {
//            self.view.endEditing(true)
//        }
//    }
//
//    // Listing of All Font Installed/Supported by System
//    func fontName() {
//        for family in UIFont.familyNames {
//            print("\(family)")
//
//            for name in UIFont.fontNames(forFamilyName: family) {
//                print("   \(name)")
//            }
//        }
//    }
//
//    // Give Alpha Animation to the Selected View
//    func setAlphaAnimation(selectedView: UIView, alpha: CGFloat) {
//        if alpha == 1 {
//            selectedView.isHidden = false
//        }
//
//        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//            selectedView.alpha = alpha
//        }) {
//            (complete) -> Void in
//            if alpha == 0 {
//                selectedView.isHidden = true
//            }
//        }
//    }
//
//    /// Check if device is registered for remote notifications for current app (read-only).
//    public static var isRegisteredForRemoteNotifications: Bool {
//        return UIApplication.shared.isRegisteredForRemoteNotifications
//    }
//
//    //MARK: - WebService Call
//    func webServiceCall(_ url: String, param:[String:Any] = [String: Any](), isWithLoading: Bool = true, loaderColor: UIColor = #colorLiteral(red: 0, green: 0.7058823529, blue: 0.6666666667, alpha: 1), imageKey: [String] = ["image"], imageData: [Data] = [],imageName:[String] = [], videoKey: [String] = ["video"], videoData: [Data] = [Data](), audioKey: [String] = ["audio"], audioData: [Data] = [Data](), isNeedToken: Bool = true, isforFuelStation: Bool = false, isforFSLocation : Bool =  false ,headerparamFS:[String:Any] = [String:Any](), methods: HTTPMethod = .post, completionHandler:@escaping ServiceResponse) {
//
//        print("URL :- \(url)")
//        print("Parameter :- \(param)")
//
//        if isReachable {
//            if isWithLoading {
//                showLoading(loaderColor)
//            }
//
//            var headers = HTTPHeaders()
//
//
//            if isforFSLocation{
//
//                headers = [
//                    "apikey": (headerparamFS["apikey"] as! String) , "transactionid" : (headerparamFS["transactionid"] as! String),"requesttimestamp" : (headerparamFS["requesttimestamp"] as! String) , "Content-Type" : (headerparamFS["Content-Type"] as! String) , "Authorization"  : (headerparamFS["Authorization"] as! String)
//                ]
//                print(headers)
//                //                headers["apikey"] = (headerparamFS["apikey"] as! String)
//                //                headers["transactionid"] = (headerparamFS["transactionid"] as! String)
//                //                headers["requesttimestamp"] = (headerparamFS["requesttimestamp"] as! String)
//                //                headers["Content-Type"] = (headerparamFS["Content-Type"] as! String)
//                //                headers["Authorization"] = (headerparamFS["Authorization"] as! String)
//            }
//
//            if isforFuelStation {
//                headers = [
//                    "accept": "application/json"
//                ]
//                headers["authorization"] = "Basic \(FuelStation.keyforAccessToken)"
//            }
//
//            if isNeedToken {
//                headers = [
//                    "accept": "application/json"
//                ]
//                headers["Authorization"] = "Bearer \(token)"
//            }
//
//            print("HTTPHeaders :- \(headers) ")
//
//            if imageData.count > 0 || videoData.count > 0 || audioData.count > 0 {
//
//                Alamofire.upload (
//                    multipartFormData: { multipartFormData in
//
//                        for (key, value) in param {
//                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
//                        }
//
//                        for i in 0..<imageData.count {
//                            if imageData[i].count > 0 {
//
//                                let fileName = imageName.count > i ? imageName[i]:"file[\(i)]"
//                                let fileName2 = imageName.count > i ? imageName[i]:"file\(i)"
//                                let strFilename = fileName
//                                multipartFormData.append(imageData[i], withName: imageKey[i], fileName: strFilename, mimeType: "image/png")
//                            }
//                        }
//
//                        for i in 0..<videoData.count {
//                            if videoData[i].count > 0 {
//                                multipartFormData.append(videoData[i], withName: videoKey[i], fileName: "file.mp4", mimeType: "video/mp4")
//                            }
//                        }
//
//                        for i in 0..<audioData.count {
//                            if audioData[i].count > 0 {
//                                multipartFormData.append(audioData[i], withName: audioKey[i], fileName: "file.m4a", mimeType: "audio/m4a")
//                            }
//                        }
//                },
//                    to: url,
//                    headers : headers,
//                    encodingCompletion: { encodingResult in
//                        switch encodingResult {
//                        case .success(let upload, _, _):
//                            upload.responseJSON { result in
//
//                                if let httpError = result.result.error {
//
//                                    print(NSString(data: result.data!, encoding: String.Encoding.utf8.rawValue)!)
//                                    print(httpError._code)
//
//                                    let response: [String: Any] = [
//                                        "errorCode": httpError._code,
//                                        "status": false,
//                                        "message": ValidationMessage.somthingWrong
//                                    ]
//
//                                    let json = JSON(response)
//                                    completionHandler(json, nil)
//
//                                    print("JSON: - \(json)")
//                                }
//
//                                if  result.result.isSuccess {
//                                    if let response = result.result.value {
//                                        let json = JSON(response)
//                                        completionHandler(json, nil)
//                                        print("JSON: - \(json)")
//                                    }
//                                }
//
//                                if isWithLoading {
//                                    self.hideLoading()
//                                }
//                            }
//                        case .failure(let encodingError):
//                            print(encodingError)
//                        }
//                })
//            } else {
//
//                Alamofire.request(url, method: methods ,parameters: param,headers: headers)
//                    .responseJSON {  result in
//
//                        if let httpError = result.result.error {
//                            print(NSString(data: result.data!, encoding: String.Encoding.utf8.rawValue)!)
//                            print(httpError._code)
//
//                            let response: [String: Any] = [
//                                "errorCode": httpError._code,
//                                "status": false,
//                                "message": ValidationMessage.somthingWrong
//                            ]
//
//                            let json = JSON(response)
//                            completionHandler(json, nil)
//
//                            print("JSON: - \(json)")
//                        }
//
//                        if  result.result.isSuccess {
//                            if let response = result.result.value {
//                                let json = JSON(response)
//                                completionHandler(json, nil)
//                                print("JSON: - \(json)")
//                            }
//                        }
//
//                        if isWithLoading {
//                            self.hideLoading()
//                        }
//                }
//            }
//        }
//        else {
//            let response: [String: Any] = [
//                "errorCode": "",
//                "status": false,
//                "message": "Internet not available"
//            ]
//
//            let json = JSON(response)
//            completionHandler(json, nil)
//        }
//    }
//}
