//
//  SearchResultTableViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 27/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {

    
    @IBOutlet weak var vendorName: UILabel!
    
    @IBOutlet weak var vendorAddress: UILabel!
   
    
    @IBOutlet weak var vendorImageView: UIImageView!
    
    @IBOutlet weak var lblOption: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
  
    
    @IBOutlet weak var lblServiceCount: UILabel!
    
    
    @IBOutlet weak var queueCount: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var nextAvialabaleHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblDateHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mainView.layer.cornerRadius = 15
        mainView.clipsToBounds = true
        mainView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
