//
//  NotifyMeCollectionViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 13/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class NotifyMeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var outerView: UIView!
    
    
    
    @IBOutlet weak var innerView: UIView!
    
    
    @IBOutlet weak var firstLbl: UILabel!
    
    @IBOutlet weak var secondLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        outerView.layer.cornerRadius = 77/2
        outerView.clipsToBounds = true
        outerView.layer.masksToBounds = true
        
        innerView.layer.cornerRadius = 67/2
        innerView.clipsToBounds = true
        innerView.layer.masksToBounds = true
    }

}
