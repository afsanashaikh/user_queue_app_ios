//
//  NotificationTableViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 17/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var vendorImageView: UIImageView!
    
    
    @IBOutlet weak var lblVendorName: UILabel!
    
    
    @IBOutlet weak var lblRequestStatus: UILabel!
    
    
    @IBOutlet weak var lblReason: UILabel!
    
    
    @IBOutlet weak var reasonViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblStatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        vendorImageView.layer.cornerRadius = 8
        vendorImageView.clipsToBounds = true
        vendorImageView.layer.masksToBounds = true
        
        mainView.layer.cornerRadius = 15
        mainView.clipsToBounds = true
        mainView.layer.masksToBounds = true
        //  mainView.layer.borderWidth = 0.5
        
        lblStatus.layer.cornerRadius = 5
        lblStatus.clipsToBounds = true
        lblStatus.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
