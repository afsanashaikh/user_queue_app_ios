//
//  constant.swift
//  User_Queue_App
//
//  Created by Mac on 15/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

var baseUrl: String = "http://35.176.9.83/"
var  send_Otp: String = "User/UserSendOTP"
var verify_Otp: String = "User/UserVerifyOTP"
var register_User:String = "User/RegisterUser"
var get_Recent_search_Result:String = "/user/GetRecentQueueVendors"
var GetSearchedAreaByUser:String = "/user/GetSearchedAreaByUser"
var GetVendorsSearch:String = "/user/GetVendorsSearchLatLong"
var GetVendorsDetails:String = "/user/GetVendorsDetails"
var GetVendorsQueueDetails:String = "/user/GetVendorsQueueDetails"
var GetVendorsServiceDetails:String = "/user/GetVendorsServiceDetails"
var GetVendorsOpeningHourDetails:String = "/user/GetVendorsOpeningHourDetails"

var GetVendorQueueDetails:String = "/user/GetVendorQueueDetails"
var UserJoinQueue:String = "/user/UserJoinQueue"
var GetUserAppointments:String = "/user/GetUserAppointments"


//UserJoinQueue
//GetVendorsInfo
//#define  get_labels @"/interests/get_labels"
