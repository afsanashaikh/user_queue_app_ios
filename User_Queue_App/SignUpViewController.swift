//
//  SignUpViewController.swift
//  User_Queue_App
//
//  Created by Mac on 05/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import CountryList


class SignUpViewController: UIViewController ,CountryListDelegate ,UITextFieldDelegate{

    @IBOutlet weak var btnGetOTP: UIButton!
    
    
    @IBOutlet weak var lblPhoneNumber: UILabel!
 
    @IBOutlet weak var lblMsg: UILabel!
    
    
    @IBAction func btnClearClicked(_ sender: Any)
    {
        txtFieldPhoneNumber.text = ""
    }
    
    
    
    
    @IBOutlet weak var countyCodeView: UIView!
    
    @IBOutlet weak var lblCountryFlag: UILabel!
    
    @IBOutlet weak var phoneNumberView: UIView!
    
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    
    @IBOutlet weak var lblCountryCode: UILabel!
   
    
    @IBOutlet weak var lblTermsAndConditions: UILabel!
    
    var countryList = CountryList()
   
    @IBOutlet weak var flagImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        btnGetOTP.layer.cornerRadius = 10.0
        countyCodeView.layer.cornerRadius = 10.0
        
        phoneNumberView.layer.cornerRadius = 10.0

        lblTermsAndConditions.attributedText = attributedText()
        
     //   picker.countryPickerDelegate = self
        
        countryList.delegate = self
        
        txtFieldPhoneNumber.delegate = self
        
       changeLabel()
        
        
        self.navigationController?.isNavigationBarHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
    }
    
    
    
    func changeLabel() {
        lblPhoneNumber.text = getLabel(langId: "1", labelId: "Phone Number")
        //print(lblJoinQueue.text as!String)
        
        
        lblMsg.text = getLabel(langId: "1", labelId: "We will send you a verification code for confirmation")
        
        btnGetOTP.setTitle(getLabel(langId: "1", labelId: "Get OTP"), for: .normal)
        
        lblTermsAndConditions.text = getLabel(langId: "1", labelId: "By providing your phone number, i hereby accept and  agree the Terms & Conditions and Privacy Policy in use of appname")
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    //method to hide keyboard
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    func attributedText() -> NSAttributedString {
        
        let string = lblTermsAndConditions.text as! NSString
        
        let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedString.Key.foregroundColor : colorWithHexString(hexString: "#9BA2B5")])
        
        
        let boldFontAttribute = [NSAttributedString.Key.foregroundColor :colorWithHexString(hexString: "#57968B") ]
        
        // Part of string to be bold
        attributedString.addAttributes(boldFontAttribute, range: string.range(of: "Terms & Conditions"))
        attributedString.addAttributes(boldFontAttribute, range: string.range(of: "Privacy Policy"))
        
        // 4
        return attributedString
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    @IBAction func btnGetOTPClicked(_ sender: Any) {
        
       checkForValidation()
        
    }
    func checkForValidation()
    {
        if txtFieldPhoneNumber.text?.isEmpty ?? true
        {
           Toast(Title: "", Text: "Please enter valid mobile number", delay: 10)
        }
        else
        {
          //  let mobileNumber = txtFieldPhoneNumber.text
           dismissKeyboard()
           call_Get_Otp()
        }
    }
    @IBAction func btnOpenCountryPickerClicked(_ sender: Any)
    {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)

        
    }
    
    
    
    func Toast(Title:String ,Text:String, delay:Int) -> Void {
        let alert = UIAlertController(title: Title, message: Text, preferredStyle: .alert)
        self.present(alert, animated: true)
        let deadlineTime = DispatchTime.now() + .seconds(delay)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
    }
    
    func call_Get_Otp()
    
    {
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        var myString = lblCountryCode.text as! String
        myString.remove(at: myString.startIndex)
        
        print(myString)
        
        let mobileNumber = myString + txtFieldPhoneNumber.text!
      //  let mobileNumber =  "918149447991"
        
         UserDefaults.standard.set(mobileNumber, forKey:"mobileNumber" )
        let hashKey = " "
        let type = 2
        
        Param["MobileNo"] = mobileNumber
        Param["HashKey"] = hashKey
        Param["Type"] = type
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+send_Otp)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+send_Otp+"?" )! , method: .post, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Status"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                       /* if let dataDict = dictionary["data"] as? [[String:Any]]
                        {
                            self.mealDate = dataDict
                            print("mealDate:\(self.mealDate)")
                            
                            DispatchQueue.main.async {
                                self.collViewHeight.constant = 300
                                self.mealCollView.reloadData()
                                self.mealCollView.layoutIfNeeded()
                                self.collViewHeight.constant = self.mealCollView.contentSize.height
                            }
                        }*/
                        
                      /*  let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objVc=storyBoard .instantiateViewController(withIdentifier: "PhoneVerificationViewController")
                        self.present(objVc, animated: true, completion: nil)*/
                        
                        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let objVc:PhoneVerificationViewController =
                            stroyboard.instantiateViewController(withIdentifier: "PhoneVerificationViewController") as!
                        PhoneVerificationViewController
                        
                        objVc.phoneNumber = self.txtFieldPhoneNumber.text
                        objVc.countryCode = self.lblCountryCode.text
                        
                        let otpString: String = (dictionary["Message"] as? String)!
                        var otpStringArr = otpString.components(separatedBy: ".")
                        
                        objVc.otpString = otpStringArr[1]
                        
                    self.navigationController?.isNavigationBarHidden = true
                    self.navigationController?.pushViewController(objVc, animated: true)
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                         //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                   // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
               // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
    }
    
    // a picker item was selected
    func selectedCountry(country: Country) {
        lblCountryCode.text = "+" + " " + country.phoneExtension
        lblCountryFlag.text = country.flag
        flagImageView.isHidden = true
    }
}
