//
//  RegistrationViewController.swift
//  User_Queue_App
//
//  Created by Mac on 08/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController ,UITextFieldDelegate {

    
    @IBOutlet weak var txtFieldFirstName: UITextField!
    
    @IBOutlet weak var txtFieldLastName: UITextField!
    
    @IBOutlet weak var btnProceed: UIButton!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var maxLen:Int = 20;

    
    @IBOutlet weak var lblWelcome: UILabel!
    
    @IBOutlet weak var lblPleaseEnterYourName: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        txtFieldLastName.layer.cornerRadius = 10.0
        txtFieldFirstName.layer.cornerRadius = 10.0
        //txtFieldFirstName.layer.borderColor = colorWithHexString(hexString: "#") as! CGColor
        txtFieldFirstName.layer.borderWidth = 0.1
        txtFieldLastName.layer.borderWidth = 0.1
        
        btnProceed.layer.cornerRadius = 10.0
        
        txtFieldFirstName.delegate = self
        txtFieldLastName.delegate = self
        
        
        //Add Tap Gesture
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        changeLabel()
        checkForValidation()
        
        
    }
    func changeLabel() {
        lblWelcome.text = getLabel(langId: "1", labelId: "Welcome To NewKyu")
        //print(lblJoinQueue.text as!String)
        
        
        lblPleaseEnterYourName.text = getLabel(langId: "1", labelId: "Please Enter Your Name")
        
        btnProceed.setTitle(getLabel(langId: "1", labelId: "Proceed"), for: .normal)
        
       txtFieldLastName.placeholder = getLabel(langId: "1", labelId: "First  Name")
       txtFieldLastName.placeholder = getLabel(langId: "1", labelId: "Last Name")
        
    }
    func checkForValidation()
    {
        if txtFieldFirstName.text?.isEmpty ?? true {
            btnProceed.isEnabled = false
            btnProceed.backgroundColor = colorWithHexString(hexString: "#D7E5E2")
            
            Toast(Title: "", Text: "Please Enter First Name", delay: 10)
        }
        else if txtFieldLastName.text?.isEmpty ?? true
        {
            btnProceed.isEnabled = false
            btnProceed.backgroundColor = colorWithHexString(hexString: "#D7E5E2")
            
               Toast(Title: "", Text: "Please Enter Last Name", delay: 10)
        }
        else
        {
            btnProceed.isEnabled = true
            
             btnProceed.backgroundColor = colorWithHexString(hexString: "#57968B")
            
           
        }
    }
    //method to hide keyboard
    
    @objc func dismissKeyboard() {
        scrollView.contentOffset.y = 0
        checkForValidation()
        view.endEditing(true)
    }
    
    @IBAction func btnProcessedClicked(_ sender: Any) {
        
        User_Register_Api_Call()
        
        
        
    //    HomeScreenVC *homeScreen = [[HomeScreenVC alloc]initWithNibName:@"HomeScreenVC" bundle:nil];
    }
    
    
    func Toast(Title:String ,Text:String, delay:Int) -> Void {
        let alert = UIAlertController(title: Title, message: Text, preferredStyle: .alert)
        self.present(alert, animated: true)
        let deadlineTime = DispatchTime.now() + .seconds(delay)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
    }
    
    func User_Register_Api_Call() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        var dict = [String: Any]()
        
        
        let firstName = txtFieldFirstName.text
        let lastName = txtFieldLastName.text
        let gender = " "
        let emailId = " "
        let mobileNumber = UserDefaults.standard.value(forKey: "mobileNumber")
        let address = " "
        let profilePic = " "
        let lat = " "
        let long = " "
        let preflang = " "
        //  let mobileNumber = self.phoneNumber
        //  let mobileNumber =  "918149447991"
        
        //  let otpString = self.txtfield1.text + self.txtfield2.text + self.txtfield3.text + self.txtField4.text self.txtField5.text + self.txtField6.text
        //let type = 2
        
        Param["FirstName"] = firstName
        Param["LastName"] = lastName
        Param["GenderId"] = gender
        Param["EmailId"] = emailId
        Param["MobileNo"] = mobileNumber
        Param["Address"] = address
        Param["ProfilePic"] = profilePic
        Param["Latitude"] = lat
        Param["Longitude"] = long
        Param["PrefLangId"] = preflang
        
        
        
        dict["UserRegistration"] = Param
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+register_User)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+register_User+"?" )! , method: .post, param: dict) { (response) in
            
           
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
               
                
               
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        UserDefaults.standard.set(dictionary["Response"], forKey: "UserId")
                        
                        print(UserDefaults.standard.value(forKey: "UserId"))
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
                        
                        self.navigationController?.isNavigationBarHidden = true
                        self.navigationController?.pushViewController(objVc, animated: true)
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       scrollView.contentOffset.y = 50
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == txtFieldFirstName){
            let currentText = textField.text! + string
            return currentText.count <= maxLen
        }
        
        if(textField == txtFieldLastName){
            let currentText = textField.text! + string
            return currentText.count <= maxLen
        }
        
        return true;
    }
}

