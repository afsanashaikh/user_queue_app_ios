//
//  AppDelegate.swift
//  User_Queue_App
//
//  Created by Mac on 05/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GooglePlaces
import GoogleMaps


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,UINavigationControllerDelegate {

    var getLabelArray = NSArray()
    var window: UIWindow?
    var navigationController: UINavigationController?
    //var hasLoggedIn: Bool?
    
    let hasLoggedIn = UserDefaults.standard.bool(forKey: "hasloggedin")
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        multiLingual_Labels()
        
    //    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor clearColor], NSForegroundColorAttributeName,  nil]];
        
        
  //  GMSPlacesClient.provideAPIKey("AIzaSyCtLGIMMU5Kik_3kfcj0RU8CwrouLGqkeU")
    
        GMSPlacesClient.provideAPIKey("AIzaSyCtLGIMMU5Kik_3kfcj0RU8CwrouLGqkeU")
        GMSServices.provideAPIKey("AIzaSyCtLGIMMU5Kik_3kfcj0RU8CwrouLGqkeU")
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

 /*  func multiLingual_Labels(){
        
        // let otpapiurl = "?MobileNo="
        
        let params = ["LangId":"1"] as [String : Any]
        
        
        //let params = ["MobileNo":"91\(txtPhoneNumber.text!)\(txtPhoneNumber.text!)\(txtPhoneNumber.text!)\(txtPhoneNumber.text!)\(txtPhoneNumber.text!)"] as [String : Any]
        
        
        Alamofire.request("http://35.177.105.49/common/GetLangTranslation", method: .get ,parameters: params, encoding: URLEncoding.methodDependent).responseJSON { (response) in
            
            
            print(response)
            
            let dictResponse = response.result.value as! NSDictionary
            print(dictResponse)
          
                if let data = dictResponse.object(forKey: "Response") as? NSArray{
                    self.getLabelArray = (data.mutableCopy() as! NSMutableArray) as! [Any]
                }
                
               // UserDefaults.standard.setValue(data1, forKey: "langresp") //imp
                print(self.getLabelArray)
                
                let launchedBefore = UserDefaults.standard.bool(forKey: "haslaunched")
                
                // let haslogedInBefore =  UserDefaults.standard.bool(forKey: "hasloggedIn")
                
                
                self.window = UIWindow(frame: UIScreen.main.bounds)
                
                let launchStoryboard = UIStoryboard(name: "Onborading", bundle: nil)
                
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                
                var vc = UIViewController()
                
                if launchedBefore
                {
                    if self.hasLoggedIn
                    {
                        vc = mainStoryBoard.instantiateViewController(withIdentifier: "DashboardViewController")
                        
                        
                        //   vc = mainStoryBoard.instantiateViewController(withIdentifier: "QueueDetailsViewController")
                        
                    }
                    else
                    {
                        
                        vc = mainStoryBoard.instantiateViewController(withIdentifier: "SignUpViewController")
                    }
                    
                    
                    // vc = launchStoryboard.instantiateViewController(withIdentifier: "FirstViewController")
                    
                }
                else
                {
                    vc = launchStoryboard.instantiateViewController(withIdentifier: "FirstViewController")
                }
                
                UserDefaults.standard.set(true, forKey: "haslaunched")
                self.window? .rootViewController = vc
                self.window?.makeKeyAndVisible()
                
                
                
            }
            //else{
            // print(data1["Message"].string!)
            // }
            
            
        }
    */
    
    func multiLingual_Labels()
    
    {
        
       // ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
       // let userid = 12
        
        Param["LangId"] = 1
        //  Param["Type"] = type
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+get_Recent_search_Result)")
        
        Webservices.dataTask_POST(Foundation.URL(string:"http://35.176.9.83/common/GetLangTranslation" )! , method: .post, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSArray{
//                            let dictResponse = response.result.value as! NSDictionary
//                            print(dictResponse)
//
                           
                                self.getLabelArray =  data.mutableCopy() as! NSMutableArray
                            
                            
                            // UserDefaults.standard.setValue(data1, forKey: "langresp") //imp
                            print(self.getLabelArray)
                            
                            let launchedBefore = UserDefaults.standard.bool(forKey: "haslaunched")
                            
                            // let haslogedInBefore =  UserDefaults.standard.bool(forKey: "hasloggedIn")
                            
                            
                            self.window = UIWindow(frame: UIScreen.main.bounds)
                            
                            let launchStoryboard = UIStoryboard(name: "Onborading", bundle: nil)
                            
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            
                            var vc = UIViewController()
                            
                            if launchedBefore
                            {
                                if self.hasLoggedIn
                                {
                                    vc = mainStoryBoard.instantiateViewController(withIdentifier: "DashboardViewController")
                                    
                                    
                                    //   vc = mainStoryBoard.instantiateViewController(withIdentifier: "QueueDetailsViewController")
                                    
                                }
                                else
                                {
                                    
                                    vc = mainStoryBoard.instantiateViewController(withIdentifier: "SignUpViewController")
                                }
                                
                                
                                // vc = launchStoryboard.instantiateViewController(withIdentifier: "FirstViewController")
                                
                            }
                            else
                            {
                                
                                
                               
                                vc = launchStoryboard.instantiateViewController(withIdentifier: "FirstViewController")
                                
                                
                                
                            }
                            
                            UserDefaults.standard.set(true, forKey: "haslaunched")
                            
                            let navigationVC = UINavigationController(rootViewController: vc)
                            //appdelegate.window!.rootViewController = navigationVC
                           
                            self.window? .rootViewController = navigationVC
                            // self.navigationController?.pushViewController(vc, animated: true)
                            self.window?.makeKeyAndVisible()
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                
                
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    
    
    
    
    
    
    }
    
    

    


extension UIViewController {
    
    
}
