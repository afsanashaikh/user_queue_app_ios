//
//  SecondViewController.swift
//  User_Queue_App
//
//  Created by Mac on 05/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController,CAAnimationDelegate {

    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
 
    
    @IBOutlet weak var lblShortDesciption: UILabel!
    
  
    
    @IBOutlet weak var lblLongDescription: UILabel!
    
    
    
    
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "ThirdViewController") as? ThirdViewController {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        
        if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       // btnNext.layer.cornerRadius = 100/2
       
       let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        
        changeLabel()
       
        
    }
    func changeLabel() {
        lblShortDesciption.text = getLabel(langId: "1", labelId: "Advance Booking")
        // print(lblJoinQueue.text as!String)
        
        
        lblLongDescription.text = getLabel(langId: "1", labelId: "Book in well advance and Rest, we will notify you just before your turn")
        btnNext.setTitle(getLabel(langId: "1", labelId: "Next"), for: .normal)
        btnSkip.setTitle(getLabel(langId: "1", labelId: "Skip"), for: .normal)
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer)
    {
        if (sender.direction == .left)
        {
            print("Swipe Left")
            
            // show the view from the right side
            
            if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "ThirdViewController") as? ThirdViewController {
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(presentVC, animated: true)
            }
        }
        
        if (sender.direction == .right)
        {
            print("Swipe Right")
            
            
            
            
            let transition = CATransition()
            transition.duration = 0.45
            transition.timingFunction = CAMediaTimingFunction(name: .default)
            transition.type = .push
            transition.subtype = .fromLeft
            transition.delegate = self
                self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            
                if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController {
                self.navigationController?.isNavigationBarHidden = true
               self.navigationController?.pushViewController(presentVC, animated: true)
                
               
            }

            // show the view from the left side
        }
    }


}
