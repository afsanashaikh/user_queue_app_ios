//
//  DaysCollectionViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 08/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class DaysCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblDay: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var bkView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bkView.layer.cornerRadius = 10.0
        bkView.clipsToBounds = true
    }

}
