//
//  QueueDetailsViewController.swift
//  User_Queue_App
//
//  Created by Mac on 11/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class QueueDetailsViewController: UIViewController ,UIScrollViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource{
    
    
    //Declare strings
    
    

    @IBOutlet weak var commentsFromVendorView: UIView!
    
    @IBOutlet weak var commentsFromVendorViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var venderImageView: UIImageView!
    
    @IBOutlet weak var lblVenderName: UILabel!
    
    @IBOutlet weak var reasonsView: UIView!
    
    @IBOutlet weak var reasonsViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var btnIamOutConfirm: UIButton!
    
    
    
    
    
    @IBOutlet weak var lblVenderAddress: UILabel!
    
    @IBOutlet weak var queueDate: UILabel!
    
    @IBOutlet weak var lblQueueFor: UILabel!
    
    @IBOutlet weak var lblYourOtp: UILabel!
    
    
    @IBOutlet weak var lblStatus: UILabel!
    
    
    @IBOutlet weak var lblOtp: UILabel!
    
    @IBOutlet weak var lblShowOtp: UILabel!
    
    @IBOutlet weak var lblAppointmentTime: UILabel!
    
    @IBOutlet weak var lblAppointmentAt: UILabel!
    
   
    @IBOutlet weak var lblQueuePosition: UILabel!
    
    @IBOutlet weak var lblTh: UILabel!
    
    
    @IBOutlet weak var lblQueueAt: UILabel!
    
    
    @IBOutlet weak var lblNotifyMe: UILabel!
    
    
    @IBOutlet weak var lblNotifyTiming: UILabel!
    
    
    @IBOutlet weak var topView: UIView!
    
    
    @IBOutlet weak var viewExpectedTime: UIView!
    
    @IBOutlet weak var viewExpectedTimeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var notifyMeView: UIView!
    
    @IBOutlet weak var viewNotifyMeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnIamOut: UIButton!
   
    
    @IBOutlet weak var btnIWillBeLate: UIButton!
    
    
    @IBOutlet weak var rescheduleView: UIView!
    
    @IBOutlet weak var rescheduleViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var iamLateView: UIView!
    
    @IBOutlet weak var iamlateViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var iwillbeLateCollectionView: UICollectionView!
    
    
    @IBOutlet weak var btnReschedule: UIButton!
    
    
    @IBOutlet weak var iwillBeLatePopupView: UIView!
    
    
    @IBOutlet weak var btnIWillBeLateConfirm: UIButton!
    
    @IBOutlet weak var btnIWillBeLateCancel: UIButton!
    
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var notufyMePopupView: UIView!
    
    @IBOutlet weak var mintsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var noOfPersonsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var btnNotifyMeConfirm: UIButton!
    
    @IBOutlet weak var btnNotifyMeCancel: UIButton!
    
    @IBOutlet weak var txtViewReason: UITextView!
    
    @IBOutlet weak var iamOutView: UIView!
    
    @IBOutlet weak var reasonTableView: UITableView!
    
    @IBOutlet weak var btnIAmOutCancel: UIButton!
    
    
    @IBOutlet weak var sepratorView: UIView!
    
    @IBOutlet weak var btnNotifyMe: UIButton!
    
    
    
    var venderName: String!
    var venderAddress: String!
    var venderImage: String!
    var queueDateStr: String!
    var queueFor: String!
    var status: String!
    var expectedTime: String!
    var expectedORAppointment: String!
    var queuePosition: String!
    var queueLabel: String!
    var topViewbackGroundcolor: String!
    var buttonsBorderColor: String!

    var iWillBelateArray : [[String:String]] = [[ : ]]
    var mintsArray :[String] = []
    var noOfPersonsArray :[String] = []
     var reasonsArray :[String] = []
    
    var selectedIndexPath : Int!
    var mintSelectedIndexPath : Int!
    var noOfPersonsselectedIndexPath : Int!
    var reasonsSelectedIndexPath : Int!
    var placeholderText = " Start typing"
    
    @IBAction func btnIwIllBeLateConfirmClicked(_ sender: Any)
    {
        var refreshAlert = UIAlertController(title: "Alert", message: "Are you sure you want to update?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
            
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(objVc, animated: true)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            
            self.backgroundView.isHidden = true
            self.iwillBeLatePopupView.isHidden = true
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnNotifyMeCancelClicked(_ sender: Any)
    {
        backgroundView.isHidden = true
        notufyMePopupView.isHidden = true

    }
    @IBAction func btnnotifyMeConfirmedClicked(_ sender: Any) {
        var refreshAlert = UIAlertController(title: "Alert", message: "Are you sure you want to update?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
            
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(objVc, animated: true)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            
            self.backgroundView.isHidden = true
            self.notufyMePopupView.isHidden = true
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnIWillBeLateCancelClicked(_ sender: Any)
    {
        backgroundView.isHidden = true
        iwillBeLatePopupView.isHidden = true
    }
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func btnNotifyMeClicked(_ sender: Any)
    {
        
        backgroundView.isHidden = false
        notufyMePopupView.isHidden = false
    }
    
    
    
    @IBAction func btnIamOutCancelClicked(_ sender: Any) {
        backgroundView.isHidden = true
        iamOutView.isHidden = true
    }
    
    
    @IBAction func btnIamOutConfirmedClicked(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Alert", message: "Are you sure you want to update?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
            
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(objVc, animated: true)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            
            self.backgroundView.isHidden = true
            self.iamOutView.isHidden = true
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnIWillBeLateClicked(_ sender: Any)
    {
        backgroundView.isHidden = false
        iwillBeLatePopupView.isHidden = false
    }
    
    @IBAction func btnIamOutClicked(_ sender: Any) {
        
        backgroundView.isHidden = false
        iamOutView.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view
        
        setupUI()
        registerNibs()
        loadArrays()
        
        topView.isUserInteractionEnabled = true
        
        
        // create the gesture recognizer
        let TapGesture = UITapGestureRecognizer(target:self,action:#selector(self.topViewTapped))
        
        // add it to the label
        topView.addGestureRecognizer(TapGesture)
        
        venderImageView.layer.cornerRadius = 10
        venderImageView.clipsToBounds = true
        venderImageView.layer.masksToBounds = true
        
    }
    
    
    @objc func topViewTapped()
    {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "VendorDetailsViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super .viewDidLayoutSubviews()
        iwillBeLatePopupView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
        notufyMePopupView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
        iamOutView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
   
    func loadArrays() {
        
        //For Days collection View
        
        
       
        
        
        iWillBelateArray =  [
            [
                "date": "Less than",
                "day": "5 min",
                
                ],
            [
                "date": "05-15",
                "day": "min",
                ],
            [
                "date": "15-30",
                "day": "min",
                ],
            [
                "date": "More than",
                "day": "15 min",
                ]
            
        ]
            
        mintsArray = ["5","10","15","20","25","30"]
        noOfPersonsArray = ["1","2","3","4","5","6"]
        
        reasonsArray = ["Plan changed","Found another vendor","Queue is moving too slow","Other"]

        
        
        
        
    }
    func setupUI(){
        
       
        
        txtViewReason.text = placeholderText//Search area, locality
        txtViewReason.textColor = UIColor .lightGray
        
        lblVenderName.text = venderName
        lblVenderAddress.text = venderAddress
       
        lblQueueFor.text = queueFor
        lblStatus.text = status
        lblAppointmentAt.text = expectedORAppointment
        lblQueueAt.text = queueLabel
        
        if queuePosition .isEmpty
        {
              lblQueuePosition.text = expectedTime
            lblTh.isHidden = true
        }
        else
        {
            lblQueuePosition.text = queuePosition
            lblTh.isHidden = false
        }
       
        if status == "Approved"
        {
             commentsFromVendorView.isHidden = true
             commentsFromVendorViewHeight.constant = 0
            viewExpectedTime.isHidden = false
            notifyMeView.isHidden = false
            viewNotifyMeHeight.constant = 80
             viewExpectedTimeHeight.constant = 80
            
            btnIamOut.layer.cornerRadius = 10
            btnIamOut.clipsToBounds = true
            btnIamOut.layer.masksToBounds = true
            btnIamOut.layer.borderColor = colorWithHexString(hexString: "#57968B").cgColor
            btnIamOut.layer.borderWidth = 1
            
            btnIWillBeLate.layer.cornerRadius = 10
            btnIWillBeLate.clipsToBounds = true
            btnIWillBeLate.layer.masksToBounds = true
            btnIWillBeLate.layer.borderColor = colorWithHexString(hexString: "#57968B").cgColor
            btnIWillBeLate.layer.borderWidth = 1
            
            topView.backgroundColor = colorWithHexString(hexString: "#57968B")
            rescheduleView.isHidden = true
            rescheduleViewHeight.constant = 0
            iamLateView.isHidden = false
            iamlateViewHeight.constant = 60
            
            lblNotifyMe.isUserInteractionEnabled = true
            btnNotifyMe.isUserInteractionEnabled = true
            
        }
        
       else if status == "Pending" {
            commentsFromVendorView.isHidden = true
            commentsFromVendorViewHeight.constant = 0
            viewExpectedTime.isHidden = false
             notifyMeView.isHidden = false
             viewNotifyMeHeight.constant = 80
            viewExpectedTimeHeight.constant = 80
            lblStatus.backgroundColor = colorWithHexString(hexString: "#FDF5DC")
                lblStatus.textColor = colorWithHexString(hexString: "#D89246")
            
            
            btnIamOut.layer.cornerRadius = 10
            btnIamOut.clipsToBounds = true
            btnIamOut.layer.masksToBounds = true
            btnIamOut.layer.borderColor = colorWithHexString(hexString: "#D89246").cgColor
            btnIamOut.layer.borderWidth = 1
            btnIamOut.titleLabel?.textColor = colorWithHexString(hexString: "#D89246")
            btnIamOut.setTitleColor(colorWithHexString(hexString: "#D89246"), for: .normal)
            
            
            btnIWillBeLate.layer.cornerRadius = 10
            btnIWillBeLate.clipsToBounds = true
            btnIWillBeLate.layer.masksToBounds = true
            btnIWillBeLate.layer.borderColor = colorWithHexString(hexString: "#D89246").cgColor
            btnIWillBeLate.layer.borderWidth = 1
            btnIWillBeLate.titleLabel?.textColor = colorWithHexString(hexString: "#D89246")
            btnIWillBeLate.setTitleColor(colorWithHexString(hexString: "#D89246"), for: .normal)

            
            topView.backgroundColor = colorWithHexString(hexString: "#D89246")
            rescheduleView.isHidden = true
            rescheduleViewHeight.constant = 0
            iamLateView.isHidden = false
            iamlateViewHeight.constant = 60
            
            lblNotifyMe.isUserInteractionEnabled = false
            btnNotifyMe.isUserInteractionEnabled = false
            
        }
        
        else
             {
                commentsFromVendorView.isHidden = false
                commentsFromVendorViewHeight.constant = 60
                viewExpectedTime.isHidden = true
                 notifyMeView.isHidden = true
                viewNotifyMeHeight.constant = 0
                viewExpectedTimeHeight.constant = 0
                sepratorView.isHidden = true
                
                lblStatus.backgroundColor = colorWithHexString(hexString: "#FFEBEA")
                lblStatus.textColor = colorWithHexString(hexString: "#E96F76")
                
             /*   btnIamOut.layer.cornerRadius = 10
                btnIamOut.clipsToBounds = true
                btnIamOut.layer.masksToBounds = true
                btnIamOut.layer.borderColor = colorWithHexString(hexString: "#57968B").cgColor
                btnIamOut.layer.borderWidth = 1
                
                btnIWillBeLate.layer.cornerRadius = 10
                btnIWillBeLate.clipsToBounds = true
                btnIWillBeLate.layer.masksToBounds = true
                btnIWillBeLate.layer.borderColor = colorWithHexString(hexString: "#57968B").cgColor
                btnIWillBeLate.layer.borderWidth = 1*/
                
                topView.backgroundColor = colorWithHexString(hexString: "#F45B6B")
                
                rescheduleView.isHidden = false
                rescheduleViewHeight.constant = 60
                iamLateView.isHidden = true
                iamlateViewHeight.constant = 0
        }
        lblStatus.layer.cornerRadius = 5
        lblStatus.clipsToBounds = true
        lblStatus.layer.masksToBounds = true
        
        topView.layer.cornerRadius = 30
        topView.clipsToBounds = true
        topView.layer.masksToBounds = true
        
        btnReschedule.layer.cornerRadius = 10
        btnReschedule.clipsToBounds = true
        btnReschedule.layer.masksToBounds = true
        
        btnIWillBeLateConfirm.layer.cornerRadius = 10
        btnIWillBeLateConfirm.clipsToBounds = true
        btnIWillBeLateConfirm.layer.masksToBounds = true
        
        btnIWillBeLateCancel.layer.cornerRadius = 10
        btnIWillBeLateCancel.clipsToBounds = true
        btnIWillBeLateCancel.layer.masksToBounds = true
       
        backgroundView.isHidden = true
        iwillBeLatePopupView.isHidden = true
        notufyMePopupView.isHidden = true
        iamOutView.isHidden = true
       
        btnNotifyMeConfirm.layer.cornerRadius = 10
        btnNotifyMeConfirm.clipsToBounds = true
        btnNotifyMeConfirm.layer.masksToBounds = true
        
        btnIamOutConfirm.layer.cornerRadius = 10
        btnIamOutConfirm.clipsToBounds = true
        btnIamOutConfirm.layer.masksToBounds = true
        
        
        reasonsView.isHidden = true
        reasonsViewHeight.constant = 0
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    func registerNibs(){
        iwillbeLateCollectionView.register(UINib(nibName: "PopUpsCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "Cell")
   
        self.iwillbeLateCollectionView.allowsSelection  = true;
        
        
        mintsCollectionView.register(UINib(nibName: "NotifyMeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "Cell")
        
        self.mintsCollectionView.allowsSelection  = true;
        
        noOfPersonsCollectionView.register(UINib(nibName: "NotifyMeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "Cell")
        
        self.noOfPersonsCollectionView.allowsSelection  = true;
        
        
         reasonTableView.register(UINib(nibName: "ReasonsTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return reasonsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReasonsTableViewCell
       
            cell.selectionStyle = .none
        let str = reasonsArray[indexPath.row]
        cell.lblReason.text = str
        
        if reasonsSelectedIndexPath == indexPath.row
        {
            cell.lblReason.textColor = colorWithHexString(hexString: "#57968B")
        }
        else
        {
            cell.lblReason.textColor = colorWithHexString(hexString: "#3E4149")
        }
        return cell
        }
    
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath)
    {
         let str = reasonsArray[indexPath.row]
        
        reasonsSelectedIndexPath = indexPath.row
        if str == "Other"
        {
            reasonsView.isHidden = false
            reasonsViewHeight.constant = 200
        }
        else{
            reasonsView.isHidden = true
            reasonsViewHeight.constant = 0
        }
        
        reasonTableView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath:  IndexPath) -> UICollectionViewCell {
        
        if collectionView == mintsCollectionView {
            
            let cell : NotifyMeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! NotifyMeCollectionViewCell
            
           

            let dayStr = mintsArray[indexPath.item]
            cell.secondLbl.text = dayStr

            if mintSelectedIndexPath == indexPath.item
            {
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#4EA191")
                //cell.firstLbl.textColor = UIColor .white
                cell.secondLbl.textColor = UIColor .white
                
              
                
           
                
                
           //     var gradientView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 35))
              /*  let gradientLayer:CAGradientLayer = CAGradientLayer()
                gradientLayer.frame.size = cell.innerView.frame.size
                gradientLayer.colors =
                    [UIColor.white.cgColor,UIColor.red.withAlphaComponent(1).cgColor]
                //Use diffrent colors
                cell.outerView.layer.addSublayer(gradientLayer)*/
                

            }
            else
            {
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
                //cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
                cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
                
              /*  let gradientLayer:CAGradientLayer = CAGradientLayer()
                gradientLayer.frame.size = cell.innerView.frame.size
                gradientLayer.colors =
                    [UIColor.lightGray.cgColor,UIColor.lightGray.withAlphaComponent(1).cgColor]
                //Use diffrent colors
                cell.outerView.layer.addSublayer(gradientLayer)*/

            }
            return cell
            
        }
        else   if collectionView == noOfPersonsCollectionView {
            
            let cell : NotifyMeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! NotifyMeCollectionViewCell
            
            let dayStr = mintsArray[indexPath.item]
            cell.secondLbl.text = dayStr
            
            if noOfPersonsselectedIndexPath == indexPath.item
            {
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#57968B")
                //cell.firstLbl.textColor = UIColor .white
                cell.secondLbl.textColor = UIColor .white
                
                
            }
            else
            {
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
                //cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
                cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
                
            }
            return cell
            
        }
        else
        {
        let cell : PopUpsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! PopUpsCollectionViewCell
        
        let dateStr = iWillBelateArray[indexPath.item]["date"]
        cell.firstLbl.text = dateStr
        
        let dayStr = iWillBelateArray[indexPath.item]["day"]
        cell.secondLbl.text = dayStr
        
       if selectedIndexPath == indexPath.item
       {
        cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
        cell.innerView.backgroundColor = colorWithHexString(hexString: "#57968B")
        cell.firstLbl.textColor = UIColor .white
        cell.secondLbl.textColor = UIColor .white

        }
        else
       {
        cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
        cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
        cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
        cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
        
        }
        return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == mintsCollectionView
        {
            return mintsArray.count
        }
        else if collectionView == noOfPersonsCollectionView
        {
            return noOfPersonsArray.count
        }
        else
        {
        return iWillBelateArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == mintsCollectionView
        {
           
                mintSelectedIndexPath = indexPath.item
                
                mintsCollectionView.reloadData()
           
        }
        else if collectionView == noOfPersonsCollectionView
        {
           
                noOfPersonsselectedIndexPath = indexPath.item
                
                noOfPersonsCollectionView.reloadData()
           
        }
        else{
       
           selectedIndexPath = indexPath.item
            
            iwillbeLateCollectionView.reloadData()
       
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text == placeholderText {
            textView.text = ""
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholderText
        }
    }
    
    
    
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
