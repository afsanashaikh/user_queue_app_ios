//
//  SearchVendorTableViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 16/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SearchVendorTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var vendorImageView: UIImageView!
    
    
    @IBOutlet weak var lblVendorName: UILabel!
    
    
    @IBOutlet weak var lblVendorAddress: UILabel!
    
    @IBOutlet weak var lblQueueCount: UILabel!
    
    @IBOutlet weak var lblServiceCount: UILabel!
  
    
    @IBOutlet weak var lastVisitedDate: UILabel!
    
    
    @IBOutlet weak var lblServiceName: UILabel!
    
    
    @IBOutlet weak var lblNextAvailableSlot: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        vendorImageView.layer.cornerRadius = 8
        vendorImageView.clipsToBounds = true
        vendorImageView.layer.masksToBounds = true
        
        mainView.layer.cornerRadius = 15
        mainView.clipsToBounds = true
        mainView.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
