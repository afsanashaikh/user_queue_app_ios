//
//  FirstViewController.swift
//  User_Queue_App
//
//  Created by Mac on 05/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController ,UINavigationControllerDelegate{

    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var lblJoinQueue: UILabel!
   
    
    @IBOutlet weak var lblDiscription: UILabel!
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        
      
        
        
        if let presentVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
    
    
    @IBAction func btnNextClicked(_ sender: Any) {
        

        
        if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(presentVC, animated: true)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//pageControl.frame = CGRect (x: pageControl.frame., y: <#T##CGFloat#>, width: <#T##CGFloat#>, height: <#T##CGFloat#>)
        
        //Left swipe
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        
        
        
        
        changeLabel()
        
    }
    
    func changeLabel() {
        lblJoinQueue.text = getLabel(langId: "1", labelId: "Join Queues From Home")
        print(lblJoinQueue.text as!String)
        
        
        lblDiscription.text = getLabel(langId: "1", labelId: "Join outdoor queues at your comfort , No need to stand in-line")
        btnNext.setTitle(getLabel(langId: "1", labelId: "Next"), for: .normal)
        btnSkip.setTitle(getLabel(langId: "1", labelId: "Skip"), for: .normal)
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer)
    {
        if (sender.direction == .left)
        {
            print("Swipe Left")
            
            // show the view from the right side
            
            // self.performSegue(withIdentifier: "goTOSecondPage", sender:self)
            
            if let presentVC = UIStoryboard(name: "Onborading", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(presentVC, animated: true)
            }
        }
        
        if (sender.direction == .right)
        {
            print("Swipe Right")
            
            // show the view from the left side
            
            
        }
    }

}
