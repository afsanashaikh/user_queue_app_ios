//
//  VendorDetailsViewController.swift
//  User_Queue_App
//
//  Created by Mac on 18/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class VendorDetailsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UIGestureRecognizerDelegate {
   
    

    @IBOutlet weak var daysCollectionView: UICollectionView!
    
    
    @IBOutlet weak var slotsCollectionView: UICollectionView!
    
    @IBOutlet weak var btnNotifyMeConfirm: UIButton!
    
    @IBOutlet weak var btnNotifyMeCancel: UIButton!
    
    
    
    @IBOutlet weak var slotsCollectionViewHeight: NSLayoutConstraint!

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    
    var positivevalue = 1
    var negativeValue = 1
    
    
    
    
    var daysArray: [[String:String]] = [[ : ]]
    //var slotsArray:[String] = []
    var queueDetailsArray = NSMutableArray()
    var servicesArray =  NSArray()
    var notifyMasterArray =  NSArray()
    var slotsArray =  NSArray()
    var mintsArray = NSArray()
  //  var noOfPersonsArray = NSArray()
     var fromServiceFlag = false
    var defaultId:Int!
    var serviceId:Int!
      var noOfPersonsArray :[String] = []
    
    var mintSelectedIndexPath : Int!
    var slotsSelectedIndexPath : Int!
    var selectedSlotIaArray = NSArray()
    var slotArray: [Any] = []

    
    var noOfPersonsselectedIndexPath : Int!
     var venderId: Int!
    //var monthStr:String!
   // var yearStr :String!
    var notifyId :Any!
     var defaultNoOfPersons:Any!
    @IBOutlet weak var btnJoin: UIButton!
    
    var arrDays = [String]()
    var newarrDates = [String]()
    var monthStr:String!
    var yearStr :String!
    var monthInt :Int!
    var yearInt :Int!
    var currentDateStr :String!
    var selectedDateStr :String!
    var selectedIndexPath : Int!
    var fromCollectionView = false
    
    @IBOutlet weak var btnCancel: UIButton!
  
     @IBOutlet weak var lblMonth: UILabel!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var notifyMePopUpView: UIView!
    
    
    
    @IBOutlet weak var mintsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var noOfPersonsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var servicesTableView: UITableView!
    
    @IBOutlet weak var servicesTableViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var topView: UIView!
    
    
    @IBOutlet weak var txtFieldServices: UITextField!
    
    @IBOutlet weak var lblQueueName: UILabel!
    
    @IBOutlet weak var txtViewDiscription: UITextView!
    
    
    @IBOutlet weak var txtFieldNoOfPersons: UITextField!
    
    @IBOutlet weak var txtFieldMintsBefore: UILabel!
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadArrays()
        registerNibs()
        
       
        
        
        let currentDate = Date()
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM'-'dd'-'yyyy"
        
        currentDateStr = dateFormatter1.string(from: currentDate)
        
        var currentStrArr = currentDateStr.components(separatedBy: "-")
        var currentDate1 = currentStrArr[1]
        
        // getLabel(langId: "1", labelId: "Last Visited")
        
        if let monthInt = Calendar.current.dateComponents([.month], from: Date()).month {
            monthStr = Calendar.current.monthSymbols[monthInt-1]
            print(monthInt) // 4
            print(monthStr) // April
            
            lblMonth.text = monthStr
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            yearStr = formatter.string(from: NSDate() as Date)
            print(yearStr)
            yearInt = Int(yearStr)
            
            lblMonth.text = monthStr + " " + yearStr
            
            let arrDatesInGivenMonthYear = getAllDates(month:monthInt, year: yearInt)
            print(arrDatesInGivenMonthYear)
            print(arrDays)
            print(newarrDates)
            
            print(arrDays.count)
            print(newarrDates.count)
            for i in 0..<self.newarrDates.count
            {
                let dateStr = newarrDates[i]
                //var myString: String = "hello hi";
                var dateStrArr = dateStr.components(separatedBy: "-")
                
                let todaysDate = dateStrArr[1]
                
                if currentDate1 ==  todaysDate
                {
                    selectedIndexPath = i
                }
                
            }
            daysCollectionView.reloadData()
            
              get_Vender_QueueDetails()
         
            
          
            
            //lblMonth.text = monthStr + " " + yearStr
        }
        
        btnJoin.layer.cornerRadius = 10
        btnJoin.clipsToBounds = true
        btnJoin.layer.masksToBounds = true
        
        btnCancel.layer.cornerRadius = 10
        btnCancel.clipsToBounds = true
        btnCancel.layer.masksToBounds = true
        
        
        
        btnNotifyMeConfirm.layer.cornerRadius = 10
        btnNotifyMeConfirm.clipsToBounds = true
        btnNotifyMeConfirm.layer.masksToBounds = true
        
        btnNotifyMeCancel.layer.cornerRadius = 10
        btnNotifyMeCancel.clipsToBounds = true
        btnNotifyMeCancel.layer.masksToBounds = true
        
        backgroundView.isHidden = true
        notifyMePopUpView.isHidden = true
        
        
        
        
        
        loadArrays()
      
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.delegate = self
        scrollView.addGestureRecognizer(tap)
          txtFieldNoOfPersons.delegate = self
        
        createPickerView()
        dismissPickerView()
        
    }
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: daysCollectionView) ?? false || (touch.view?.isDescendant(of: slotsCollectionView))! {
            
            // Don't let selections of auto-complete entries fire the
            // gesture recognizer
            return false
        }
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFieldNoOfPersons
        {
            view.endEditing(true)
            
            return true
        }
        return false
    }
    override func viewDidLayoutSubviews() {
        super .viewDidLayoutSubviews()
        topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 30.0)
        topView.backgroundColor = colorWithHexString(hexString: "#57968B")
        notifyMePopUpView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
        
        if fromCollectionView == false
        {
        let indexPath = IndexPath(item: selectedIndexPath, section: 0)
        self.daysCollectionView.scrollToItem(at: indexPath, at: [.centeredVertically,   .centeredHorizontally], animated: false)
        self.daysCollectionView.backgroundColor =  colorWithHexString(hexString: "#E7E7E7")
        }
        
        
    }
    
    func registerNibs(){
        daysCollectionView.register(UINib(nibName: "DaysCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "DaysCollectionViewCell")
        
      slotsCollectionView.register(UINib(nibName: "SlotsAvailableCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "Cell")
        
      
            
            
            mintsCollectionView.register(UINib(nibName: "NotifyMeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "Cell")
            
            self.mintsCollectionView.allowsSelection  = true;
            
            noOfPersonsCollectionView.register(UINib(nibName: "NotifyMeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "Cell")
            
            self.noOfPersonsCollectionView.allowsSelection  = true;
            
            
        
        
    }
    
    
    @IBAction func btnDropDownClicked(_ sender: Any)
    {
        
//        servicesTableView.reloadData()
//        servicesTableViewHeight.constant = servicesTableView.contentSize.height
     
    }
    func createPickerView() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        txtFieldServices.inputView = pickerView
      //  scrollView.addSubview(pickerView)
        
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
       // let button = UIBarButtonItem(title: “Done”, barButtonSystemItem: .plain, target: self, action: #selector(self.action))
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.action) )
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        txtFieldServices.inputAccessoryView = toolBar
    }
    @objc func action() {
        view.endEditing(true)
    }
    func get_Vender_QueueDetails() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
      //  let userid = 12
      //  let venderId = 24
        if fromServiceFlag == true
        {
           Param["ServiceId"] = serviceId
        }
        else
        {
             Param["ServiceId"] = ""
        }
        
        if fromCollectionView == true
        {
            Param["Date"] = selectedDateStr
        }
        else
        {
            Param["Date"] = currentDateStr
        }
        
        Param["VendorQueueId"] = venderId
      //  Param["ServiceId"] = ""
        Param["DayId"] = ""
        
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetVendorQueueDetails)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetVendorQueueDetails+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSDictionary{
                          //  self.queueDetailsArray = data.mutableCopy() as! NSMutableArray
                            
                            self.lblQueueName.text = data.value(forKey: "QueueName")as? String
                            self.lblDescription.text = data.value(forKey: "ShortDesc")as? String
                            
                            self.servicesArray = data.value(forKey: "Services") as! NSArray
                            print(self.servicesArray)
                            
                            if self.fromServiceFlag == true
                            {
                                
                            }
                            else
                            
                            {
                                let dict = self.servicesArray[0] as! NSDictionary
                                
                                let ServiceName = dict.value(forKey: "ServiceName") as! String
                                
                                
                                
                                self.txtFieldServices.text = ServiceName
                            }
                            self.notifyMasterArray = data.value(forKey: "NotifyMaster") as! NSArray
                            self.slotsArray = data.value(forKey: "Slots") as! NSArray
                            print(self.slotsArray)
                            print(self.slotsArray.count)
                            
                            
                            
                            
                            if self.slotsArray != nil  // if your array is not empty refresh the tableview
                            {
                               self.slotsCollectionView.reloadData()
                               
                                self.slotsCollectionViewHeight.constant = self.slotsCollectionView.contentSize.height
                                
                                print(self.slotsCollectionViewHeight.constant)
                            }
                            else
                            
                            {
                                self.slotsCollectionViewHeight.constant = 0
                                
                            }
                            
                            
                            
                           /*if self.slotsArray.count > 0
                            {
                             
                            }
                            else
                            {
                                self.slotsCollectionViewHeight.constant = 0
                                self.slotsCollectionView.reloadData()
                            }*/
                             self.slotsCollectionView.reloadData()
                            self.mintsCollectionView.reloadData()
                            
                            
                            self.defaultId = data.value(forKey: "DefaultNotiId")as! Int
                            self.defaultNoOfPersons = data.value(forKey: "DefaultPersonBefore")as! Int
                            
                            for i in 0..<self.notifyMasterArray.count
                            {
                                let dict = self.notifyMasterArray[i] as! NSDictionary
                                
                                var ServiceId = dict.value(forKey: "Id") as! Int
                                
                                if ServiceId == self.defaultId
                                {
                                    //let ServiceIdStr: String = "\(ServiceId)"

                                    let servicename = dict.value(forKey: "Name") as! String
                                    
                                    self.txtFieldMintsBefore.text = servicename + " mints before"
                                }
                            }
                        
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            self.Toast(Title: "", Text: dictionary["Message"]as! String, delay: 5)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                    ActivityIndicator.stopIndicator()
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                ActivityIndicator.stopIndicator()
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    
    
    func join_Queue() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var joinQueueParam = [String: Any]()
        var Param = [String:Any]()
        
        var userjoinQueue = [String: Any]()
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
        //  let userid = 12
       // let venderId = 24
        if fromServiceFlag == true
        {
            joinQueueParam["ServiceId"] = serviceId
        }
        else
        {
            joinQueueParam["ServiceId"] = ""
        }
        joinQueueParam["VendorQueueId"] = venderId
        
        
        
        let userid = UserDefaults.standard.value(forKey: "UserId")
        
        if fromCollectionView == true
        {
            joinQueueParam["AppoDateTime"] =  selectedDateStr
        }
        else
        {
            joinQueueParam["AppoDateTime"] =  currentDateStr
        }
        
       
        joinQueueParam["VendorQueueId"] = venderId
        joinQueueParam["UserId"] = userid
       // Param["ServiceId"] = serviceId
       
        
        
        Param["UserJoinQueue"] = joinQueueParam
        
        Param["SlotId"] = slotArray
        Param["NotiId"] = notifyId
        Param["PersonBefore"] = defaultNoOfPersons
        Param["PersonWithMe"] = txtFieldNoOfPersons.text
        
        
        print("Param is \(Param)")
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+UserJoinQueue)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+UserJoinQueue+"?" )! , method: .post, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        
                            let messageStr = dictionary["Message"] as? String
                            self.Toast(Title: "", Text: messageStr! , delay: 5)
                            //Message
                            
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
                        
                        self.navigationController?.isNavigationBarHidden = true
                        self.navigationController?.pushViewController(objVc, animated: true)
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            //print(dictionary["Message"]as! String)
                            let messageStr = dictionary["Message"] as? String
                            self.Toast(Title: "", Text: messageStr! , delay: 5)
                            
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                    ActivityIndicator.stopIndicator()
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                
                ActivityIndicator.stopIndicator()
                ActivityIndicator.stopIndicator()
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
                
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(objVc, animated: true)
                
                break
            default:
                break
            }
        }
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
        
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
        
    }
    
    func Toast(Title:String ,Text:String, delay:Int) -> Void {
        let alert = UIAlertController(title: Title, message: Text, preferredStyle: .alert)
        self.present(alert, animated: true)
        let deadlineTime = DispatchTime.now() + .seconds(delay)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
    }
    
    @IBAction func btnNotifyMeClicked(_ sender: Any)
    {
        
        backgroundView.isHidden = false
        notifyMePopUpView.isHidden = false
    }
    
    func loadArrays() {
        
        //For Days collection View
        
        daysArray = [
            [
                "date": "05",
                "day": "Sun",
                
                ],
            [
                "date": "06",
                "day": "Mon",
                ],
            [
                "date": "07",
                "day": "Tue",
                ],
            [
                "date": "08",
                "day": "Wed",
                ],
            [
                "date": "09",
                "day": "Thu",
                ],
            [
                "date": "10",
                "day": "Fri",
                ],
            [
                "date": "11",
                "day": "Sat",
                ]
        ]
        
        
      //  slotsArray = ["10:00","10:30","11:00","11:30","12:00","12:30"]
        
        mintsArray = ["5","10","15","20","25","30"]
        noOfPersonsArray = ["1","2","3","4","5","6"]
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath:  IndexPath) -> UICollectionViewCell {
        
        if collectionView == daysCollectionView
        {
            
            let cell : DaysCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DaysCollectionViewCell", for: indexPath as IndexPath) as! DaysCollectionViewCell
            
            let dateStr = newarrDates[indexPath.item]
            //var myString: String = "hello hi";
            var dateStrArr = dateStr.components(separatedBy: "-")
            
            cell.lblDate.text = dateStrArr[1]
            
            let dayStr = arrDays[indexPath.item]
            cell.lblDay.text = dayStr
            
            
            
            
            
            
            if selectedIndexPath == indexPath.item
            {
                
                cell.bkView.backgroundColor = UIColor .white
                cell.lblDay.textColor = colorWithHexString(hexString: "#57968B")
                cell.lblDate.textColor = colorWithHexString(hexString: "#57968B")
                
            }
            else
            {
                
                cell.bkView.backgroundColor = UIColor .clear
                cell.lblDay.textColor = colorWithHexString(hexString: "#5A5858")
                cell.lblDate.textColor = colorWithHexString(hexString: "#5A5858")
              
                
            }
            //        if indexPath.item == 3 {
            //            cell.bkView.backgroundColor = UIColor .white
            //            cell.lblDay.textColor = colorWithHexString(hexString: "#57968B")
            //            cell.lblDate.textColor = colorWithHexString(hexString: "#57968B")
            //        }
            return cell
            
        }
      else  if collectionView == mintsCollectionView {
            
            let cell : NotifyMeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! NotifyMeCollectionViewCell
            
            let dict = notifyMasterArray[indexPath.item]as! NSDictionary
            
            let dayStr = dict.value(forKey: "Name")as! String
            cell.secondLbl.text = dayStr
            
            if mintSelectedIndexPath == indexPath.item
            {
                
                let ServiceId = dict.value(forKey: "Id") as! Int
                
                if ServiceId == self.defaultId
                {
                    cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
                    cell.innerView.backgroundColor = colorWithHexString(hexString: "#4EA191")
                    //cell.firstLbl.textColor = UIColor .white
                    cell.secondLbl.textColor = UIColor .white
                }
                else
                {
                    cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
                    cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
                    //cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
                    cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
                }
                
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#4EA191")
                //cell.firstLbl.textColor = UIColor .white
                cell.secondLbl.textColor = UIColor .white
                
                
                let dict = self.notifyMasterArray[mintSelectedIndexPath] as! NSDictionary
                
                notifyId = dict.value(forKey: "Id")
                
                
                
                //     var gradientView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 35))
                /*  let gradientLayer:CAGradientLayer = CAGradientLayer()
                 gradientLayer.frame.size = cell.innerView.frame.size
                 gradientLayer.colors =
                 [UIColor.white.cgColor,UIColor.red.withAlphaComponent(1).cgColor]
                 //Use diffrent colors
                 cell.outerView.layer.addSublayer(gradientLayer)*/
                
                
                
                
            }
            else
            {
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
                //cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
                cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
                
                /*  let gradientLayer:CAGradientLayer = CAGradientLayer()
                 gradientLayer.frame.size = cell.innerView.frame.size
                 gradientLayer.colors =
                 [UIColor.lightGray.cgColor,UIColor.lightGray.withAlphaComponent(1).cgColor]
                 //Use diffrent colors
                 cell.outerView.layer.addSublayer(gradientLayer)*/
                notifyId = defaultId
            }
            
           
            return cell
            
        }
        else   if collectionView == noOfPersonsCollectionView {
            
            let cell : NotifyMeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! NotifyMeCollectionViewCell
            
            let dayStr = noOfPersonsArray[indexPath.item]
            cell.secondLbl.text = dayStr
            
            if noOfPersonsselectedIndexPath == indexPath.item
            {
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#57968B")
                //cell.firstLbl.textColor = UIColor .white
                cell.secondLbl.textColor = UIColor .white
                
                
            }
            else
            {
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
                //cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
                cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
                
            }
            return cell
            
        }
        else
        {
            let cell : SlotsAvailableCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! SlotsAvailableCollectionViewCell
            
            
            print(slotsArray)
        
            let dict = self.slotsArray[indexPath.row] as! NSDictionary
            
             let slotDict = dict.value(forKey: "SlotTime")as! NSDictionary
            
           // let OpeningHoursDict = dict.value(forKey: "OpenTime") as! NSDictionary
            
          //  let closingHoursDict = dict.value(forKey: "CloseTime") as! NSDictionary
            
            let openingHours = slotDict.value(forKey: "Hours") as! Int
            let openingHoursStr: String = "\(openingHours)"
            
            
            let openingmints = slotDict.value(forKey: "Minutes") as! Int
            let openingmintsStr: String = "\(openingmints)"
            
            
            let openingHoursMint = openingHoursStr + ":" + openingmintsStr
            
            cell.secondLbl.text = openingHoursMint
            
            let isselected = dict.value(forKey: "IsBook") as! Bool
            
            if isselected == true
            {
                cell.isUserInteractionEnabled = false
                cell.backgroundColor = colorWithHexString(hexString: "#f7f7f7")
               // cell.secondLbl.backgroundColor = colorWithHexString(hexString: "#f7f7f7")
            }
            else{
                cell.isUserInteractionEnabled = true
                cell.backgroundColor = UIColor.clear
                cell.secondLbl.backgroundColor = UIColor.clear
            }
            
            
            if slotsSelectedIndexPath == indexPath.item
                        {
                             let dict = self.slotsArray[slotsSelectedIndexPath] as! NSDictionary
                             let slotId = dict.value(forKey: "Id")
                           // slotArray.adding(slotId)
                            slotArray.append(slotId)
                            print(slotArray)
                            cell.secondLbl.textColor = colorWithHexString(hexString: "#57968B")
                            cell.layer.borderColor = colorWithHexString(hexString: "#738197") .cgColor
                            
                            cell.layer.borderWidth = 1.0
                            cell.layer.cornerRadius = 10
                            cell.clipsToBounds = true
                            cell.layer.borderWidth = 1.0
                            cell.layer.borderColor = colorWithHexString(hexString: "#57968B").cgColor
                            
                        }
                        else
                        {
                             cell.secondLbl.textColor = colorWithHexString(hexString: "#8d8585").withAlphaComponent(1)
                            cell.layer.borderColor = colorWithHexString(hexString: "#8d8585").withAlphaComponent(1) .cgColor
                            cell.layer.borderWidth = 1.0
                            
                            cell.layer.cornerRadius = 10
                            cell.clipsToBounds = true
                            cell.layer.borderWidth = 1.0
                            cell.layer.borderColor = UIColor .clear .cgColor
                            
                            
                        }
            
//            let dateStr = slotsArray[indexPath.item]
//            cell.secondLbl.text = dateStr
//
//            if indexPath.item == 1 && indexPath.item == 3
//            {
//                cell.secondLbl.isUserInteractionEnabled = false
//            }
//           else
//            {
//                  cell.secondLbl.isUserInteractionEnabled = true
//            }
//            if mintSelectedIndexPath == indexPath.item
//            {
//                cell.secondLbl.textColor = colorWithHexString(hexString: "#57968B")
//            }
//            else
//            {
//                 cell.secondLbl.textColor = colorWithHexString(hexString: "#8d8585").withAlphaComponent(1)
//            }
            return cell
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == daysCollectionView
        {
             return arrDays.count
        }
       else if collectionView == mintsCollectionView
        {
            return notifyMasterArray.count
        }
        else if collectionView == noOfPersonsCollectionView
        {
            return noOfPersonsArray.count
        }
            
        else
        {
            print(slotsArray.count)
            if slotsArray.count > 0
            {
                return slotsArray.count
            }
            else
            {
               return 0
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == slotsCollectionView
        {
            
            slotsSelectedIndexPath = indexPath.item
            
            slotsCollectionView.reloadData()
            
        }
       else if collectionView == mintsCollectionView
        {
            
            mintSelectedIndexPath = indexPath.item
            
            mintsCollectionView.reloadData()
            
        }
            
        else if collectionView == daysCollectionView
        {
            selectedIndexPath = indexPath.item
            daysCollectionView.reloadData()
            fromCollectionView = true
            let dateStr = newarrDates[indexPath.item]
            selectedDateStr = dateStr
          //  get_Appointments()
            
            get_Vender_QueueDetails()
            
        }
            
        else
        {
            
            noOfPersonsselectedIndexPath = indexPath.item
            
            noOfPersonsCollectionView.reloadData()
            
        }
        
    }
    
   
    
 //TableView Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return servicesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        servicesTableView.register(UINib(nibName: "RecentlySearcgedAreaCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecentlySearcgedAreaCell
        
        cell1.selectionStyle = .none
        
        print(servicesArray.count)
        let dict = servicesArray[indexPath.row] as! NSDictionary
        
        let ServiceName = dict.value(forKey: "ServiceName") as! String
        
        
        cell1.lblAreaName.text = ServiceName
        cell1.serachImgView.isHidden = true
        
        return cell1
    }
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath)
    {
        let dict = servicesArray[indexPath.row] as! NSDictionary
        
        let ServiceName = dict.value(forKey: "ServiceName") as! String
        
        self.serviceId = dict.value(forKey: "ID")as! Int
        
       txtFieldServices.text = ServiceName
        
        fromServiceFlag = true
        servicesTableViewHeight.constant = 0
        
        get_Vender_QueueDetails()
       
    }
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
  
    
    @IBAction func BtnPreviousMonthClicked(_ sender: Any)
    {
        
        let previousMonth = Calendar.current.date(byAdding: .month, value: -negativeValue, to: Date())
       // let datecomponents = Calendar.current.dateComponents([.month], from: previousMonth!)
        
        
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let monthString = dateFormatter.string(from: previousMonth!)
        
         lblMonth.text = monthString + " " + yearStr
        
        negativeValue = negativeValue + 1
    }
    
    @IBAction func btnNextMonthClicked(_ sender: Any)
    {
        
        
        
        let nextMonth = Calendar.current.date(byAdding: .month, value: positivevalue, to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let monthString = dateFormatter.string(from: nextMonth!)
        
        lblMonth.text = monthString + " " + yearStr
        
        positivevalue = positivevalue + 1
        
    }
    /*
     
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

 
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNotifyMeCancelClicked(_ sender: Any)
    {
        backgroundView.isHidden = true
        notifyMePopUpView.isHidden = true
        
    }
    @IBAction func btnnotifyMeConfirmedClicked(_ sender: Any) {
        var refreshAlert = UIAlertController(title: "Alert", message: "Are you sure you want to update?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            self.backgroundView.isHidden = true
            self.notifyMePopUpView.isHidden = true
            
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            
            
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnJoinClicked(_ sender: Any)
    {
        
        join_Queue()
        
    }
    @IBAction func btnCancelClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAllDates(month: Int, year: Int) -> [Date] {
        let dateComponents = DateComponents(year: year, month: month)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy'-'MM'-'dd"
        //     formatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        var arrDates = [Date]()
        newarrDates = [String]()
        arrDays = [String]()
        for day in 1...numDays {
            let dateString = "\(year) \(month) \(day)"
            
            
            if let date = formatter.date(from: dateString)
            {
                
                print(date)
                arrDates.append(date)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEE"
                let dayInWeek = dateFormatter.string(from: date)
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "MM'-'dd'-'yyyy"
                
                let newdate = dateFormatter1.string(from: date)
                print(newdate)
                
                
                
                newarrDates.append(newdate)
                // arrDates.append(newdate)
                arrDays.append(dayInWeek)
                print(dayInWeek)
                
                
            }
        }
        
        return arrDates
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        print(servicesArray.count)
        return  servicesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print(servicesArray)
        let dict = servicesArray[row] as! NSDictionary
        let serviceName = dict.value(forKey: "ServiceName")as! NSString
        
        return serviceName as String
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dict = servicesArray[row] as! NSDictionary
        let serviceName = dict.value(forKey: "ServiceName")as! NSString
        txtFieldServices.text = serviceName as String
        
        
       self.serviceId = dict.value(forKey: "ID")as! Int
        
        fromServiceFlag = true
        servicesTableViewHeight.constant = 0
        
        get_Vender_QueueDetails()
        
    }

}


extension Date {
    func getNextMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: 1, to: self)
    }
    
    func getPreviousMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -1, to: self)
    }
}
