//
//  WebServices.swift
//  User_Queue_App
//
//  Created by Mac on 14/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

import Alamofire
import SwiftyJSON

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class Webservices: NSObject {
    
    
    static fileprivate let kTimeOutInterval:Double = 20
    
    static fileprivate var sharedAlamofire:SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = kTimeOutInterval
        configuration.timeoutIntervalForRequest =  kTimeOutInterval
        let alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        
        return alamoFireManager
    }()
    
    
    
    final class func dataTask_POST(_ path: URL, method: HTTPMethod, param: Dictionary<String, Any>, compilationBlock:@escaping (_ result: Result<Any> ) -> Void){
        
        
        print("URL FOR WEBSERVICE IS \(path)")
        //        var accessToken = ""
        //        if let accToken = UserDefaults.standard.value(forKey: "accessToken") as? String
        //        {
        //            accessToken = accToken
        //        }
        //        let headers = ["Authorization" : "Bearer "+accessToken+""]
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
        }
        
        self.sharedAlamofire.request(path, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            print(response.result)
            switch(response.result) {
            case .success(let JSON):
                compilationBlock(.success(JSON))
                break
            case .failure(_):
                let customError = NSError(domain: "Network", code: 67, userInfo: [NSLocalizedDescriptionKey : "Server Not Responding"]);
                compilationBlock(.failure(customError))
                break
                
            }
        }
    }
    
    final class func dataTask_POST_Without_Tokan(_ path: URL, method: HTTPMethod, param: Dictionary<String, Any>, compilationBlock:@escaping (_ result: Result<Any> ) -> Void){
        
        
        print("URL FOR WEBSERVICE IS \(path)")
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
        }
        
        self.sharedAlamofire.request(path, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(let JSON):
                compilationBlock(.success(JSON))
                break
            case .failure(_):
                let customError = NSError(domain: "Network", code: 67, userInfo: [NSLocalizedDescriptionKey : "Server Not Responding"]);
                compilationBlock(.failure(customError))
                break
                
            }
        }
    }
    
    final class func dataTask_GET(_ path: URL, method: HTTPMethod, param: Dictionary<String, Any>, compilationBlock:@escaping (_ result: Result<Any> ) -> Void){
        
        
        print("URL FOR WEBSERVICE IS \(path)")
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
        }
        //        var accessToken = ""
        //        if let accToken = UserDefaults.standard.value(forKey: "accessToken") as? String
        //        {
        //            accessToken = accToken
        //        }
        //        let headers = ["Authorization" : "Bearer "+accessToken+""]
        
        
        self.sharedAlamofire.request(path, method: .get, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(let JSON):
                compilationBlock(.success(JSON))
                break
            case .failure(_):
                let customError = NSError(domain: "Network", code: 67, userInfo: [NSLocalizedDescriptionKey : "Server Not Responding"]);
                compilationBlock(.failure(customError))
                break
                
            }
        }
    }
    
    final class func dataTask_DELETE(_ path: URL, method: HTTPMethod, param: Dictionary<String, Any>, compilationBlock:@escaping (_ result: Result<Any> ) -> Void){
        
        
        print("URL FOR WEBSERVICE IS \(path)")
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
        }
        
        self.sharedAlamofire.request(path, method: .delete, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(let JSON):
                compilationBlock(.success(JSON))
                break
            case .failure(_):
                let customError = NSError(domain: "Network", code: 67, userInfo: [NSLocalizedDescriptionKey : "Server Not Responding"]);
                compilationBlock(.failure(customError))
                break
                
            }
        }
    }
    
    final class func post_RAW_Data( path: URL, method: HTTPMethod, param: Dictionary<String, Any>, compilationBlock:@escaping ( _ result: Result<[String:Any]> ) -> Void){
        
        
        print("URL FOR WEBSERVICE IS \(path)")
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
        }
        
        var  jsonData = NSData()
        
        do{
            jsonData = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted) as NSData
            print("json data is \(jsonData)")
        } catch {
            print(error.localizedDescription)
        }
        
        let url:URL = path
        let session = URLSession.shared
        //        var accessToken = ""
        //        if let accToken = UserDefaults.standard.value(forKey: "accessToken") as? String
        //        {
        //            accessToken = accToken
        //        }
        //
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("\(jsonData.length)", forHTTPHeaderField: "Content-Length")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //        request.setValue("Bearer "+accessToken+"", forHTTPHeaderField: "Authorization")
        request.httpBody = jsonData as Data
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            print("data isss \(data)")
            
            guard let data = data, let _:URLResponse = response, error == nil else {
                print("error")
                return
            }
            
            //    let dataString =    String(data: data, encoding: String.Encoding.utf8)
            do {
                
                let dataString = String(data: data, encoding: String.Encoding.utf8)
                print("data get fetched",dataString ?? "no data")
                let parsedData = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                let pasrsedData1 = parsedData as! [String:Any]
                print("parsedData is \(pasrsedData1)")
                
                compilationBlock(Result.success(pasrsedData1))
                
            }catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
    
    
    //    func requestPostMethod(_ method: HTTPMethod
    //        , _ URLString: String
    //        , parameters: [String : AnyObject]? = [:]
    //        , headers: [String : String]? = [:]
    //        , onView: UIView?, vc: UIViewController, completion:@escaping (Any?) -> Void
    //        , failure: @escaping (Error?) -> Void) {
    //
    //        Alamofire.request(URLString, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers)
    //            .responseJSON { response in
    //
    //
    //                switch response.result {
    //                case .success:
    //                    completion(response.result.value!)
    //                case .failure(let error):
    //                    failure(error)
    //                    guard error.localizedDescription == JSON_COULDNOT_SERIALISED else {
    //
    //                        return
    //                    }
    //                }
    //        }
    //    }
    
    
    
    
}

