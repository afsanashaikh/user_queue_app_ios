//
//  DashboardViewController.swift
//  User_Queue_App
//
//  Created by Mac on 08/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SDWebImage;

class DashboardViewController: UIViewController ,UIScrollViewDelegate, UITableViewDelegate, UICollectionViewDataSource, UITableViewDataSource, CalendarDelgate,UICollectionViewDelegateFlowLayout
{

    var daysArray: [[String:String]] = [[ : ]]
    
    var calendarArray: NSArray?
    var calendarPicker: CalendarPicker?
    var appointmentArray = NSMutableArray()
    var arrDays = [String]()
    var newarrDates = [String]()
    var monthStr:String!
    var yearStr :String!
    var monthInt :Int!
    var yearInt :Int!
    var currentDateStr :String!
    var selectedDateStr :String!
    var selectedIndexPath : Int!
    var fromCollectionView = false
    var scrolltoIndexPath : IndexPath!
    
    
    @IBOutlet weak var lblMonth: UILabel!
    
    
    @IBOutlet weak var lblMyQueues: UILabel!
    
    
    
    
    @IBOutlet weak var daysCollectionView: UICollectionView!
    
    
    
    @IBOutlet weak var queuesListTableView: UITableView!
    
    
    @IBAction func btnSerachClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "SearchVendorViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func btnNotificationClicked(_ sender: Any)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "NotificationViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func btnHomeClicked(_ sender: Any)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)    }
    
 
    @IBAction func btnOpenDatePickerClicked(_ sender: Any)
    {
      
        
        AKMonthYearPickerView.sharedInstance.barTintColor = UIColor .gray
        
        AKMonthYearPickerView.sharedInstance.previousYear = 4
        
        AKMonthYearPickerView.sharedInstance.show(vc: self, doneHandler: doneHandler, completetionalHandler: completetionalHandler)
        
    }
    
    private func doneHandler() {
        print("Month picker Done button action")
    }
    
    private func completetionalHandler(month: Int, year: Int) {
        print( "month = ", month, " year = ", year )
        if month == 1 {
            lblMonth.text = "January"
        }
        else if month == 2 {
            lblMonth.text = "February"
        }
        else if month == 3 {
            lblMonth.text = "March"
        }
        else if month == 4 {
            lblMonth.text = "April"
        }
        else if month == 5 {
            lblMonth.text = "May"
        }
        else if month == 6
        {
            lblMonth.text = "June"
        }
        else if month == 7 {
            lblMonth.text = "July"
        }
        else if month == 8
        {
            lblMonth.text = "August"
        }
        else if month == 9 {
            lblMonth.text = "September"
        }
        else if month == 10 {
            lblMonth.text = "October"
        }
        else if month == 11 {
            lblMonth.text = "November"
        }
        else
        {
            lblMonth.text = "December"
        }
        
        let arrDatesInGivenMonthYear = getAllDates(month:month, year: year)
        print(arrDatesInGivenMonthYear)
        print(arrDays)
        print(newarrDates)
        
        print(arrDays.count)
        print(newarrDates.count)
        daysCollectionView.reloadData()
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        loadArrays()
        registerNibs()
      //  let appDelegate = UIApplication.shared.delegate as! AppDelegate
       // appDelegate.hasLoggedIn = UserDefaults.standard.bool(forKey: "haslaunched")
        UserDefaults.standard.set(true, forKey: "hasloggedin")
        
       
        
        let currentDate = Date()
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM'-'dd'-'yyyy"
        
            currentDateStr = dateFormatter1.string(from: currentDate)
        
        var currentStrArr = currentDateStr.components(separatedBy: "-")
        var currentDate1 = currentStrArr[1]
       
      lblMyQueues.text = getLabel(langId: "1", labelId: "My Queues")
        queuesListTableView.backgroundColor = colorWithHexString(hexString: "#f7f7f7")
        
        print(labelName)
        
        if let monthInt = Calendar.current.dateComponents([.month], from: Date()).month {
            monthStr = Calendar.current.monthSymbols[monthInt-1]
            print(monthInt) // 4
            print(monthStr) // April
            
            lblMonth.text = monthStr
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            yearStr = formatter.string(from: NSDate() as Date)
            print(yearStr)
            yearInt = Int(yearStr)
            
            
            let arrDatesInGivenMonthYear = getAllDates(month:monthInt, year: yearInt)
            print(arrDatesInGivenMonthYear)
            print(arrDays)
            print(newarrDates)
            
            print(arrDays.count)
            print(newarrDates.count)
            for i in 0..<self.newarrDates.count
            {
                let dateStr = newarrDates[i]
                //var myString: String = "hello hi";
                var dateStrArr = dateStr.components(separatedBy: "-")
                
                let todaysDate = dateStrArr[1]
                
                if currentDate1 ==  todaysDate
                {
                    selectedIndexPath = i
                  
                }
                
            }
            
            daysCollectionView.reloadData()
            
            
            get_Appointments()
            
            
            
            //lblMonth.text = monthStr + " " + yearStr
        }
        
        /*if monthInt == Calendar.current.dateComponents([.month], from: Date()).month {
            monthStr = Calendar.current.monthSymbols[monthInt-1]
            print(monthInt) // 4
            print(monthStr) // April
            
            lblMonth.text = monthStr
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            yearStr = formatter.string(from: NSDate() as Date)
            print(yearStr)
            
           
            
            lblMonth.text = monthStr
            
            
           
            
        }*/
      
        
        navigationController?.isNavigationBarHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
    }
    @objc func dismissKeyboard() {
        
    AKMonthYearPickerView.sharedInstance.hide()
        
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        if fromCollectionView == false
        {
            let indexPath = IndexPath(item: selectedIndexPath, section: 0)
            self.daysCollectionView.scrollToItem(at: indexPath, at: [.centeredVertically,   .centeredHorizontally], animated: false)
        }
        else
        {
            
        }
    }
    
    func registerNibs(){
        daysCollectionView.register(UINib(nibName: "DaysCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "DaysCollectionViewCell")

       
     
        
    }
    
    func loadArrays() {
        
   //For Days collection View
        
        daysArray = [
            [
                "date": "05",
                "day": "Sun",
                
                ],
            [
                "date": "06",
                "day": "Mon",
                ],
            [
                "date": "07",
                "day": "Tue",
                ],
            [
                "date": "08",
                "day": "Wed",
                ],
            [
                "date": "09",
                "day": "Thu",
                ],
            [
                "date": "10",
                "day": "Fri",
                ],
            [
                "date": "11",
                "day": "Sat",
        ]
        ]
        
        
        
        
    }

    func get_Appointments() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        let userid = UserDefaults.standard.value(forKey: "UserId")
        
        Param["UserId"] = userid
        
        if fromCollectionView == true
        {
             Param["AppointmentDate"] = selectedDateStr
        }
        else
        {
             Param["AppointmentDate"] = currentDateStr
        }
       
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetUserAppointments)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetUserAppointments+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSArray
                        {
                            self.appointmentArray = data.mutableCopy() as! NSMutableArray
                            if self.appointmentArray.count>0
                            {
                               self.queuesListTableView.reloadData()
                                self.queuesListTableView.isHidden = false
                            }
                            else
                            {
                                  self.queuesListTableView.reloadData()
                                self.queuesListTableView.isHidden = true
                            }
                         
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                    ActivityIndicator.stopIndicator()
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                ActivityIndicator.stopIndicator()
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    
    func Toast(Title:String ,Text:String, delay:Int) -> Void {
        let alert = UIAlertController(title: Title, message: Text, preferredStyle: .alert)
        self.present(alert, animated: true)
        let deadlineTime = DispatchTime.now() + .seconds(delay)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
    }
    //ScrollView Methods
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath:  IndexPath) -> UICollectionViewCell {
       
        let cell : DaysCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DaysCollectionViewCell", for: indexPath as IndexPath) as! DaysCollectionViewCell
        
        let dateStr = newarrDates[indexPath.item]
        //var myString: String = "hello hi";
        var dateStrArr = dateStr.components(separatedBy: "-")
        
        cell.lblDate.text = dateStrArr[1]
        
        let dayStr = arrDays[indexPath.item]
        cell.lblDay.text = dayStr
        
       
        
        
        
        
        if selectedIndexPath == indexPath.item
        {
           
            cell.bkView.backgroundColor = UIColor .white
//            cell.bkView.layer.borderColor = colorWithHexString(hexString: "#8d8585") . cgColor
//            cell.bkView.layer.borderWidth = 1.0
            cell.lblDay.textColor = colorWithHexString(hexString: "#57968B")
            cell.lblDate.textColor = colorWithHexString(hexString: "#57968B")
            
            
        }
        else
        {
            
            cell.bkView.backgroundColor = UIColor .clear
//            cell.bkView.layer.borderColor = UIColor.clear  .cgColor
//            cell.bkView.layer.borderWidth = 1.0
            cell.lblDay.textColor = colorWithHexString(hexString: "#5A5858")
            cell.lblDate.textColor = colorWithHexString(hexString: "#5A5858")
            
        }
//        if indexPath.item == 3 {
//            cell.bkView.backgroundColor = UIColor .white
//            cell.lblDay.textColor = colorWithHexString(hexString: "#57968B")
//            cell.lblDate.textColor = colorWithHexString(hexString: "#57968B")
//        }
        return cell
    
}
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
       return arrDays.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        selectedIndexPath = indexPath.item
        daysCollectionView.reloadData()
        fromCollectionView = true
        let dateStr = newarrDates[indexPath.item]
        selectedDateStr = dateStr
        get_Appointments()
        
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return appointmentArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        queuesListTableView.register(UINib(nibName: "QueueListTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QueueListTableViewCell
        cell1.selectionStyle = .none
        cell1.contentView.backgroundColor = colorWithHexString(hexString: "#f7f7f7")
        
        
        
        let dict = appointmentArray[indexPath.row] as! NSDictionary
        
        print(dict)
        
        var statusName = dict.value(forKey: "StatusName")as! String
        
        
        let venderName = dict.value(forKey: "BusinessName")
        
        if let venderName = venderName as? String {
            // You have your String, process as needed
            
            print(venderName)
            
            cell1.lblVendorName.text = venderName as! String
            
            
            
        } else if let venderName = venderName as? NSNull {
            // It was "null", process as needed
            print("vendorAddress is null")
            
            cell1.lblVendorName.text = ""
            
        } else {
            // It is something else, possible nil, process as needed
            
            print(venderName)
        }
        
        
        let vendorAddress = dict.value(forKey: "Address")
        
        if let vendorAddress = vendorAddress as? String {
            // You have your String, process as needed
            
            print(vendorAddress)
            
            cell1.lblVendorAddress.text = vendorAddress as! String
            
            
            
        } else if let vendorAddress = vendorAddress as? NSNull {
            // It was "null", process as needed
            print("vendorAddress is null")
            
            cell1.lblVendorAddress.text = ""
            
        } else {
            // It is something else, possible nil, process as needed
            
            print(vendorAddress)
        }
        
        
        let QueueName = dict.value(forKey: "QueueName")
        
        if let QueueName = QueueName as? String {
            // You have your String, process as needed
            
            print(QueueName)
            
            cell1.lblQueueTypw.text = QueueName
            
            
            
        } else if let QueueName = QueueName as? NSNull {
            // It was "null", process as needed
            print("vendorAddress is null")
            
            cell1.lblQueueTypw.text = ""
            
        } else {
            // It is something else, possible nil, process as needed
            
            print(QueueName)
        }
        
        
        if statusName == "Pending"
        {
            let expectedTime = dict.value(forKey: "ExpectedAt")
            
            if let expectedTime = expectedTime as? String {
                // You have your String, process as needed
                
                print(expectedTime)
                
                cell1.lblExpectedTime.text = expectedTime
                
                
                
            } else if let expectedTime = expectedTime as? NSNull {
                // It was "null", process as needed
                print("vendorAddress is null")
                
                cell1.lblExpectedTime.text = ""
                
            } else {
                // It is something else, possible nil, process as needed
                
                print(expectedTime)
            }
            
            cell1.lblExpectedAt.text = "Appointment at"
            
            cell1.lblTh.isHidden = true
            cell1.lblTotalNumberOfGuest.isHidden = true
            cell1.lblQueuePosition.isHidden = true
            cell1.lblInTheQueue.isHidden = true
            cell1.lblSecond.isHidden = true
            cell1.lblTotalGuest.isHidden = true
            
            cell1.lblStatus.text = statusName
            cell1.lblSecond.isHidden = true
            cell1.lblFirst.isHidden = true
            cell1.lblStatus.backgroundColor = colorWithHexString(hexString: "#FDF5DC")
            cell1.lblStatus.textColor = colorWithHexString(hexString: "#D89246")
            cell1.mainView.backgroundColor = colorWithHexString(hexString: "#D89246")
            cell1.btnUpcoming.isHidden = true
            cell1.view2Height.constant = 60
            cell1.messageImageView.isHidden = true
            
          
            
            var profilePicUrl = dict.value(forKey: "ProfilePic")
            
            var profilePicUrlStr :String!
            if let profilePicUrl = profilePicUrl as? String {
                // You have your String, process as needed
                
                print(expectedTime)
                
               profilePicUrlStr = baseUrl + profilePicUrl
                
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))



                
//
                
            } else if let profilePicUrl = profilePicUrl as? NSNull {
                // It was "null", process as needed
                print("vendorAddress is null")
                
                profilePicUrlStr = ""
                
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))

                
            } else {
                // It is something else, possible nil, process as needed
                
            
                profilePicUrlStr = ""
                
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))


            }
            
           
        }
        else
        {
            
        }
            return cell1
    }
    
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: NSIndexPath(row: indexPath.row, section: 0) as IndexPath) as! QueueListTableViewCell

        
        let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:QueueDetailsViewController =
            storyboard?.instantiateViewController(withIdentifier: "QueueDetailsViewController") as!
        QueueDetailsViewController
        
        objVc.venderName = cell.lblVendorName.text
          objVc.venderAddress = cell.lblVendorAddress.text
          // objVc.venderImage = cell.vendorImageView.image
           objVc.expectedTime = cell.lblExpectedTime.text
           objVc.expectedORAppointment = cell.lblExpectedAt.text
           objVc.queuePosition = cell.lblQueuePosition.text
           objVc.queueLabel = cell.lblInTheQueue.text
           objVc.status = cell.lblStatus.text
           objVc.queueFor = cell.lblQueueTypw.text
        
        
            
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
        
    }
    
   /* func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == 0 {
            queuesListTableView.register(UINib(nibName: "QueueListTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! QueueListTableViewCell
        }
        else if indexPath.row == 1 {
            queuesListTableView.register(UINib(nibName: "QueueListTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! QueueListTableViewCell
        }
        else if indexPath.row == 2 {
            queuesListTableView.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! ListTableViewCell
        }
        else  {
            queuesListTableView.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! ListTableViewCell
        }
    }*/
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    
    func getCalendar() -> CalendarPicker {
        
        if calendarPicker == nil {
            calendarPicker = CalendarPicker()
            calendarPicker?.delegate = self
        }
        return calendarPicker!
    }
    
 /*   func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QueueListTableViewCell
//        cell.mainView.layer.cornerRadius = 20
//        cell.mainView.clipsToBounds = true
//        cell.mainView.layer.masksToBounds = true
    }*/

    
    
    func getAllDates(month: Int, year: Int) -> [Date] {
        let dateComponents = DateComponents(year: year, month: month)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy'-'MM'-'dd"
   //     formatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        var arrDates = [Date]()
        newarrDates = [String]()
        arrDays = [String]()
        for day in 1...numDays {
            let dateString = "\(year) \(month) \(day)"
            
          
            if let date = formatter.date(from: dateString)
            {
               
                print(date)
                arrDates.append(date)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEE"
                let dayInWeek = dateFormatter.string(from: date)
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "MM'-'dd'-'yyyy"
                
                let newdate = dateFormatter1.string(from: date)
                print(newdate)
                
               
                
                newarrDates.append(newdate)
                   // arrDates.append(newdate)
                    arrDays.append(dayInWeek)
                print(dayInWeek)

                
            }
        }
        
        return arrDates
    }
}
extension UILabel
{
    func height() -> CGFloat {
        
        let lbl = UILabel(frame: frame)
        lbl.font = font
        lbl.text = text
        lbl.numberOfLines = numberOfLines
        
        
        let constraint = CGSize(width: lbl.frame.size.width, height: CGFloat.greatestFiniteMagnitude)
        var size: CGSize
        
        let context = NSStringDrawingContext()
        let boundingBox = lbl.text?.boundingRect(
            with: constraint,
            options: .usesLineFragmentOrigin,
            attributes: [
                NSAttributedString.Key.font: lbl.font
            ],
            context: context).size
        
        size = CGSize(width: ceil((boundingBox?.width)!), height: ceil((boundingBox?.height)!))
        
        return size.height
    }

}
