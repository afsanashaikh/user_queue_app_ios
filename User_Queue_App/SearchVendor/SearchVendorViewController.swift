//
//  SearchVendorViewController.swift
//  User_Queue_App
//
//  Created by Mac on 16/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class SearchVendorViewController: UIViewController,UITabBarDelegate,UITableViewDataSource ,UITableViewDelegate,GMSMapViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate{

    
    @IBOutlet weak var noResultFoundView: UIView!
    
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var lblSerachVendors: UILabel!
    
    @IBOutlet weak var searchView: UIView!
    
    
    @IBOutlet weak var searchService: UITextField!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    
    @IBOutlet weak var tableViewSearchVendor: UITableView!
    
    @IBOutlet weak var btnSearchHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewSearchVendortop: NSLayoutConstraint!
    
    @IBOutlet weak var txtFieldSearchArea: UITextField!
    
    
    @IBOutlet weak var lblAppointment: UILabel!
    
    @IBOutlet weak var lblAppointmentHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var recentlyVistedViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var recentlyVisitedView: UIView!
    
    
    @IBOutlet weak var mapSepratorView: UIView!
    
   // @IBOutlet weak var mapView: MKMapView!
    
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblRecentlyVisited: UILabel!
    
    
    @IBOutlet weak var lblMap: UILabel!
    
    var searchFlag = false
    var mapFlag = false
    var searchButtonClickedFlag = false
    var searchVendorArray = NSMutableArray()
    var searchResultArray = NSMutableArray()
    var latArray =  NSArray()
    var longArray = NSArray()
    var businessNameArray = NSArray()
    var areaFlag = false
    
    
    var doublelat = Double()
    var doublelon = Double()
    
    @IBOutlet weak var tableViewSearchResult: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         searchView.layer.cornerRadius = 10.0
        
        btnSearch.layer.cornerRadius = btnSearchHeight.constant/2
       // btnSearch.addBorder(side: .bottom, color: UIColor.black, leftOffset: 15.0)
        btnSearch.layer.borderColor = colorWithHexString(hexString: "#57968B") .cgColor
        btnSearch.layer.borderWidth = 2.0
        txtFieldSearchArea.addTarget(self, action: #selector(myTargetFunction), for: .touchDown)
        searchService.delegate = self
        
        get_Recently_Visited()
       // self.lblAppointment.isHidden = true
      //  self.lblAppointmentHeight.constant = 0
        
        self.noResultFoundView.isHidden = true

        mapView.isHidden = true
        lblMap.isHidden = true
        recentlyVisitedView.isHidden = true
        mapSepratorView.isHidden = true
        
        
        lblRecentlyVisited.isUserInteractionEnabled = true
        
        tableViewSearchResult.backgroundColor = colorWithHexString(hexString: "#f7f7f7")
        tableViewSearchVendor.backgroundColor = colorWithHexString(hexString: "#f7f7f7")
        
        
        
        // create the gesture recognizer
        let labelRecentlyVisitedTapGesture = UITapGestureRecognizer(target:self,action:#selector(self.recentlyVisitedlabelTapped))
        
        // add it to the label
        lblRecentlyVisited.addGestureRecognizer(labelRecentlyVisitedTapGesture)
        
        
        lblMap.isUserInteractionEnabled = true
        
        
        // create the gesture recognizer
        let labelMapTapGesture = UITapGestureRecognizer(target:self,action:#selector(self.maplabelTapped))
        
        // add it to the label
        lblMap.addGestureRecognizer(labelMapTapGesture)
        
        tableViewSearchResult.isHidden = true
        tableViewSearchVendortop.constant = tableViewSearchVendortop.constant - 15
        print(UserDefaults.standard.value(forKey: "place"))
        
   //     let place = as! String
        
        if areaFlag == false
        {
            txtFieldSearchArea.text = "Search area/locality"
            txtFieldSearchArea.textColor = UIColor.lightGray //colorWithHexString(hexString: "#8d8585")
            
        }
        else
        {
                    if UserDefaults.standard.value(forKey: "place")  == nil
                    {
                        txtFieldSearchArea.text = "Search area, locality"
                        txtFieldSearchArea.textColor = colorWithHexString(hexString: "#8d8585")
            
                    }
                    else
                    {
                        txtFieldSearchArea.text = UserDefaults.standard.value(forKey: "place") as! String
                    }
        }
        
//        if UserDefaults.standard.value(forKey: "place")  == nil
//        {
//            txtFieldSearchArea.text = "Search area, locality"
//            txtFieldSearchArea.textColor = colorWithHexString(hexString: "#8d8585")
//
//        }
//        else
//        {
//            txtFieldSearchArea.text = UserDefaults.standard.value(forKey: "place") as! String
//        }
        
       
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        
        
   }
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //textField.resignFirstResponder()
        view.endEditing(true)
        return true
    }
    
    func changeLabel() {
        lblSerachVendors.text = getLabel(langId: "1", labelId: "Search Vendors")
        //print(lblJoinQueue.text as!String)
        
        
        lblRecentlyVisited.text = getLabel(langId: "1", labelId: "Recently Visited")
        
      //  lblRecentlyVisited.setTitle(getLabel(langId: "1", labelId: "Proceed"), for: .normal)
        
        txtFieldSearchArea.placeholder = getLabel(langId: "1", labelId: "Search Area/ Locality")
        searchService.placeholder = getLabel(langId: "1", labelId: "Search Service, Business")
        
    }
    
    
    @objc func recentlyVisitedlabelTapped()
    {
        lblRecentlyVisited.textColor = colorWithHexString(hexString: "#57968B")
        recentlyVisitedView.backgroundColor = colorWithHexString(hexString: "#57968B")
        
        lblMap.textColor = UIColor .lightGray
        mapSepratorView.backgroundColor = UIColor.lightGray
        mapSepratorView.isHidden = true
        recentlyVisitedView.isHidden = false
        searchFlag = true
        mapFlag = false
        
        
        mapView.isHidden = true
//        lblAppointment.isHidden = true
//        lblAppointmentHeight.constant = 0
         tableViewSearchVendor.isHidden = false
        tableViewSearchResult.isHidden = false
        
        
        
    }
    @objc func maplabelTapped()
    {
        lblMap.textColor = colorWithHexString(hexString: "#57968B")
        mapSepratorView.backgroundColor = colorWithHexString(hexString: "#57968B")
        mapSepratorView.isHidden = false
        recentlyVisitedView.isHidden = true
        
        
        
        
        lblRecentlyVisited.textColor = UIColor .lightGray
        recentlyVisitedView.backgroundColor = UIColor.lightGray
        
        
        
        searchFlag = false
        mapFlag = true
        
        
        mapView.isHidden = false
//        lblAppointment.isHidden = true
//        lblAppointmentHeight.constant = 0
//
        tableViewSearchVendor.isHidden = true
        tableViewSearchResult.isHidden = true
        
        showAllMarkers()
    }
    
    @IBAction func btnSerachClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "SearchVendorViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func btnNotificationClicked(_ sender: Any)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "NotificationViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func btnHomeClicked(_ sender: Any)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    
    @objc func myTargetFunction(textField: UITextField)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "AreaLocalityViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchButtonClickedFlag == true {
             return searchResultArray.count
        }
        else
        {
        return searchVendorArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if searchButtonClickedFlag == true
        {
            tableViewSearchResult.register(UINib(nibName: "SearchResultTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchResultTableViewCell
            cell1.selectionStyle = .none
            
            let dict = searchResultArray[indexPath.row] as! NSDictionary

            let vendorName = dict.value(forKey: "BusinessName")

            if let vendorName = vendorName as? String {
                // You have your String, process as needed
                
                print(vendorName)
                
                cell1.vendorName.text = vendorName as! String
                
                
                
            } else if let vendorName = vendorName as? NSNull {
                // It was "null", process as needed
                print("vendorAddress is null")
                
                cell1.vendorName.text = ""
                
            } else {
                // It is something else, possible nil, process as needed
                
                print(vendorName)
            }


            let vendorAddress = dict.value(forKey: "Address")

            if let vendorAddress = vendorAddress as? String {
                // You have your String, process as needed
                
                print(vendorAddress)
                
                cell1.vendorAddress.text = vendorAddress as! String
                
                
                
            } else if let vendorAddress = vendorAddress as? NSNull {
                // It was "null", process as needed
                print("vendorAddress is null")
                
                cell1.vendorAddress.text = ""
                
            } else {
                // It is something else, possible nil, process as needed
                
                print(vendorAddress)
            }

            let serviceCount = dict.value(forKey: "ServiceCount") as! Int
            //let aud : String = String(rateDict["AUD"])

       
            let serviceStr: String = "\(serviceCount)"

            if serviceCount==1
            {
             let serviceName = dict.value(forKey: "ServiceName") as! String
              cell1.lblServiceCount.text = serviceName
            }
            else
            {
               cell1.lblServiceCount.text = serviceStr + " Services"
            }
           // cell1.lblServiceCount.text = serviceStr +  " Services"


            let queueCount = dict.value(forKey: "QueueCount") as! Int

            let queueStr: String = "\(queueCount)"
            
            if queueCount == 1
            {
                 let queueName = dict.value(forKey: "QueueName") as! String
                cell1.queueCount.text = queueName
            }

          else
            {
                cell1.queueCount.text = queueStr + " Queues"
            }
            
            
            if queueCount>1
            {
                cell1.lblOption.isHidden = true
                cell1.lblDate.isHidden = true
                cell1.nextAvialabaleHeight.constant = 0
                cell1.lblDateHeight.constant = 0
            }
            else
            {
            
                cell1.lblOption.isHidden = false
                cell1.lblDate.isHidden = false
                cell1.nextAvialabaleHeight.constant = 25
                cell1.lblDateHeight.constant = 25
                
                let queueType = dict.value(forKey: "QueueType") as! Int
                
                if queueType == 1
                {
                    cell1.lblOption.text = "Next Available Slot :"
                    
                     let nextSlot = dict.value(forKey: "NextSlot")
                    
                    if let nextSlot = nextSlot as? String {
                        // You have your String, process as needed
                        
                        print(nextSlot)
                        
                        cell1.lblDate.text =  nextSlot
                        
                        
                        
                    } else if let nextSlot = nextSlot as? NSNull {
                        // It was "null", process as needed
                        print("vendorAddress is null")
                        
                       cell1.lblDate.text = ""
                        
                    } else {
                        // It is something else, possible nil, process as needed
                        
                        print(nextSlot)
                    }
                    
                    
                }
                else
                {
                    cell1.lblOption.text = "You will be :"
                    
                    let appointmentsCount = dict.value(forKey: "ExpAppNo") as! Int
                    
                    let appointmentStr: String = "\(appointmentsCount)"
                    
                    
                    if appointmentsCount > 11 && appointmentsCount < 13
                    {
                       
                        
                         cell1.lblDate.text = appointmentStr as!String + " th" + " in the queue"
                    }
                        
                    else
                    {
                        switch(appointmentsCount % 10)
                        {
                        case 1:
                            //self.lblTh.text = "st"
                            cell1.lblDate.text = appointmentStr as!String + " st" + " in the queue"
                        case 2:
                           // self.lblTh.text = "nd"
                            cell1.lblDate.text = appointmentStr as!String + " nd" + " in the queue"
                            break
                        case 3:
                           // self.lblTh.text = "rd"
                            
                            cell1.lblDate.text = appointmentStr as!String + " rd" + " in the queue"
                            break
                            
                            
                            
                        default:
                          //  self.lblTh.text = "th"
                            cell1.lblDate.text = appointmentStr as!String + " th" + " in the queue"
                            break
                        }
                    }
                    
                   
                }
                
                
            }
//
//            let lastVisited = dict.value(forKey: "LastVisited") as! String
//
//            cell1.lastVisitedDate.text = lastVisited
//
//            let quueType = dict.value(forKey: "QueueType") as! String
//
//
//            if quueType == "1"
//            {
//
//                let nextslot = dict.value(forKey: "NextSlot") as! String
//                cell1.lblNextAvailableSlot.text = "Next Available Slot :" + nextslot
//
//            }
//            else
//            {
//                cell1.lblNextAvailableSlot.text = " "
//            }
//
//
//            let vendorServices = dict.value(forKey: "VendorServices") as! NSArray
//            let serviceName:[String] = vendorServices.value(forKey: "ServiceName") as! [String]
//            print(serviceName)
//            cell1.lblServiceName.text = serviceName[0] as! String
//
            
            
            
            var profilePicUrl = dict.value(forKey: "ProfilePic")
            
            print(profilePicUrl)
            
            var profilePicUrlStr :String!
            if let profilePicUrl = profilePicUrl as? String {
                // You have your String, process as needed
                
             
                
                profilePicUrlStr = baseUrl + profilePicUrl
                
                print(profilePicUrlStr)
                
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                
                
                
                
                //
                
            } else if let profilePicUrl = profilePicUrl as? NSNull {
                // It was "null", process as needed
                print("vendorAddress is null")
                
                profilePicUrlStr = ""
                
                  print(profilePicUrlStr)
                
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                
                
            } else {
                // It is something else, possible nil, process as needed
                
                
                profilePicUrlStr = ""
                
                  print(profilePicUrlStr)
                
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                
                
            }
            
            return cell1
        }
        else
        {
        tableViewSearchVendor.register(UINib(nibName: "SearchVendorTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchVendorTableViewCell
        cell1.selectionStyle = .none
        
        let dict = searchVendorArray[indexPath.row] as! NSDictionary
        
        let vendorName = dict.value(forKey: "BusinessName") as! String
        
        cell1.lblVendorName.text = vendorName

        
//            var vendorAddress = dict.value(forKey: "Address") as! Any
//
//            print(vendorAddress)
            
            
            let vendorAddress = dict.value(forKey: "Address") // An optional
            
            print(vendorAddress)
            
            if let vendorAddress = vendorAddress as? String {
                // You have your String, process as needed
                
                print(vendorAddress)
                
                cell1.lblVendorAddress.text = vendorAddress as! String

                
                
            } else if let vendorAddress = vendorAddress as? NSNull {
                // It was "null", process as needed
                print("vendorAddress is null")
                
                cell1.lblVendorAddress.text = ""

            } else {
                // It is something else, possible nil, process as needed
                
                print(vendorAddress)
            }
        //    cell1.lblVendorAddress.text = vendorAddress as! String

        
       let serviceCount = dict.value(forKey: "ServiceCount") as! Int
        //let aud : String = String(rateDict["AUD"])
        
       
        let serviceStr: String = "\(serviceCount)"
        
        cell1.lblServiceCount.text = serviceStr +  " Services"
        
        
        let queueCount = dict.value(forKey: "QueueCount") as! Int
        
        let queueStr: String = "\(queueCount)"
        
        cell1.lblQueueCount.text = queueStr +  " Queues"
        
        let lastVisited = dict.value(forKey: "LastVisited") as! String
        let serviceName = dict.value(forKey: "ServiceName") as! String
            print(serviceName)
        
        cell1.lastVisitedDate.text = lastVisited + ", " + serviceName
        
        let quueType = dict.value(forKey: "QueueType") as! Int
        
          //  let queueCount = dict.value(forKey: "QueueCount") as! Int
            
            let queueTypeStr: String = "\(queueCount)"
            
          //  cell1.lblQueueCount.text = queueStr
            
            
        if queueTypeStr == "1"
        {
            
            let nextslot = dict.value(forKey: "NextSlot") as! String
            cell1.lblNextAvailableSlot.text = "Next Available Slot :" + " " + nextslot
        
        }
        else
        {
            let nextslot = dict.value(forKey: "NextSlot") as! String
            cell1.lblNextAvailableSlot.text = "Next Available Slot :" + " " + nextslot
        }
        
        
      //  let vendorServices = dict.value(forKey: "VendorServices") as! NSArray
        
       // cell1.lblServiceName.text = serviceName
        
            
            
            
            
            var profilePicUrl = dict.value(forKey: "ProfilePic")
            
            print(profilePicUrl)
            
            
            var profilePicUrlStr :String!
            if let profilePicUrl = profilePicUrl as? String {
                // You have your String, process as needed
                
                
                
                profilePicUrlStr = baseUrl + profilePicUrl
                
                  print(profilePicUrlStr)
                
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                
                
                
                
                //
                
            } else if let profilePicUrl = profilePicUrl as? NSNull {
                // It was "null", process as needed
                print("vendorAddress is null")
                
                profilePicUrlStr = ""
                  print(profilePicUrlStr)
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                
                
            } else {
                // It is something else, possible nil, process as needed
                
                
                profilePicUrlStr = ""
                  print(profilePicUrlStr)
                cell1.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                
                
            }
            
        return cell1
        }
    }

    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath)
    {
      
       /* let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objVc:SearchVendorDetailsVC =
            storyboard?.instantiateViewController(withIdentifier: "SearchVendorDetailsVC") as!SearchVendorDetailsVC
    self.present(objVc, animated: true, completion: nil)*/
        
        
        if tableView == tableViewSearchResult
        {
            
            let dict = searchResultArray[indexPath.row] as! NSDictionary
            print(dict)
            let vendorId = dict.value(forKey: "Id") as! Int
            
            let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let objVc:SearchVendorDetailsVC =
                storyboard?.instantiateViewController(withIdentifier: "SearchVendorDetailsVC") as!
            SearchVendorDetailsVC
           // objVc.venderId = vendorId
            objVc.venderId = vendorId
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(objVc, animated: true)
        }
        
        else
        {
            
            let dict = searchVendorArray[indexPath.row] as! NSDictionary
            print(dict)
           //
             let vendorId = dict.value(forKey: "QueueId") as! Int
            let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let objVc:VendorDetailsViewController =
                storyboard?.instantiateViewController(withIdentifier: "VendorDetailsViewController") as!
            VendorDetailsViewController
            objVc.venderId = vendorId
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(objVc, animated: true)
        }
    }
   
    override func viewDidLayoutSubviews() {
        super .viewDidLayoutSubviews()
        topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 30.0)
        
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
   
    
    func get_Recently_Visited() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
      
        
        
       let userid = UserDefaults.standard.value(forKey: "UserId")
        
       // let userid = 12
        
        Param["UserId"] = userid
        //  Param["Type"] = type
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+get_Recent_search_Result)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+get_Recent_search_Result+"?" )! , method: .post, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                      
                            if let data =  dictionary["Response"] as? NSArray{
                                self.searchVendorArray = data.mutableCopy() as! NSMutableArray
                                
                                print(self.searchVendorArray)
                                print(self.searchVendorArray.count)
                                
                                if self.searchVendorArray.count>0
                                {
                                  self.tableViewSearchVendor.reloadData()
//                                    self.lblAppointment.isHidden = true
//                                    self.lblAppointmentHeight.constant = 0
                                    self.noResultFoundView.isHidden = true

                                }
                                
                                else
                                {
//                                    self.lblAppointment.isHidden = false
//                                    self.lblAppointmentHeight.constant = 22
                                    self.tableViewSearchVendor.isHidden = true
                                    self.noResultFoundView.isHidden = false

                                }
                            }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    func get_Serach_Result() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
       // let userid = 12
        
        Param["Latitude"] = UserDefaults.standard.value(forKey: "lat")
        Param["Longitude"] = UserDefaults.standard.value(forKey: "long")
        //Param["SearchName"] = txtFieldSearchArea.text
        
        Param["SearchName"] = searchService.text

        Param["UserId"] = UserDefaults.standard.value(forKey: "UserId")
        
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetVendorsSearch)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetVendorsSearch+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSArray{
                            
                            print(data)
                        self.searchResultArray = data.mutableCopy() as! NSMutableArray

                            print(self.searchResultArray)
                            print(self.searchResultArray.count)
                            
                            self.latArray = data.value(forKey: "Latitude") as! NSArray
                            self.longArray = data.value(forKey: "Latitude")as! NSArray
                            self.businessNameArray = data.value(forKey: "BusinessName") as! NSArray
                            
                            print(self.searchResultArray)
                            print(self.searchResultArray.count)

                            print(self.latArray)
                            print(self.latArray.count)
                            
                                print(self.longArray)
                                print(self.longArray.count)
                            
                                print(self.businessNameArray)
                                print(self.businessNameArray.count)
                            
                            
                            if self.searchResultArray.count>0
                            {
                                self.tableViewSearchResult.reloadData()
//                                self.lblAppointment.isHidden = false
//                                self.lblAppointmentHeight.constant = 0
                                
                                self.noResultFoundView.isHidden = true
//
                            }

                            else
                            {
                                //self.lblAppointment.isHidden = false
                                //self.lblAppointmentHeight.constant = 22
                                self.tableViewSearchVendor.isHidden = true
                                self.tableViewSearchResult.isHidden  = true
                                self.noResultFoundView.isHidden = false

                            }
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    
                    self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                    ActivityIndicator.stopIndicator()
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    func Toast(Title:String ,Text:String, delay:Int) -> Void {
        let alert = UIAlertController(title: Title, message: Text, preferredStyle: .alert)
        self.present(alert, animated: true)
        let deadlineTime = DispatchTime.now() + .seconds(delay)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
    }
    func showAllMarkers()
    {
       /* CLGeocoder().geocodeAddressString(txtFieldSearchArea.text!, completionHandler: { placemarks, error in
            if (error != nil) {
                let alert = UIAlertController(title: "", message: "No Route Found", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            if let placemark = placemarks?[0]  {
                
                let lat = String(format: "%.04f", (placemark.location?.coordinate.latitude ?? 0.0)!)
                let lon =   String(format: "%.04f", (placemark.location?.coordinate.longitude ?? 0.0)!)
                
                let name = placemark.name!
                let country = placemark.country!
                //  let region = placemark.administrativeArea!
                print("location,\(lat),\(lon)\n\(name) \(country)")
                
              /*  self.doublelat = NumberFormatter().number(from: lat)!.doubleValue
                self.doublelon = NumberFormatter().number(from: lon)!.doubleValue
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(latitude: self.doublelat, longitude: self.doublelon)
                annotation.title = "hiii"
                self.mapView.addAnnotation(annotation)*/
 
                
            }
            
            
        }
        )*/
        
        
        for i in 0..<self.searchResultArray.count{ // n= count of array
            
            //possitioning marker,here mark,mrk,name are mutable arrays
            
            
            
            let position = CLLocationCoordinate2D(latitude: self.latArray[i] as! CLLocationDegrees, longitude: self.longArray[i] as! CLLocationDegrees)
            
            if (position.latitude > 0  && position.longitude > 0)
            {
                let marker = GMSMarker()
                let camera = GMSCameraPosition.camera(withLatitude: self.latArray[i] as! CLLocationDegrees,longitude: self.self.longArray[i] as! CLLocationDegrees,zoom: 6.0)
                // let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
                
                
                print(camera)
                print(camera)
                
                // [self.mapView animateToCameraPosition:cameraPosition];
                
                self.mapView .camera = camera
                
                self.mapView .animate(to: camera)
                
                marker.position = position
                
                
                
                print(self.businessNameArray[i])
                marker.title = self.businessNameArray[i] as? String
                //   marker.snippet = self.name[i] as! String//mutable array
                marker.map = self.mapView
                mapView.delegate = self;
            }
                
            else
            {
                let camera = GMSCameraPosition.camera(withLatitude: self.latArray[i] as! CLLocationDegrees,longitude: self.self.longArray[i] as! CLLocationDegrees,zoom: 6.0)
                // let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
                
                
                
                self.mapView .camera = camera
            }
            
        }
 
 
 }
    
    func mapView(_ mapView: GMSMapView!, didTapInfoWindowOf marker: GMSMarker!)
    {
        
        
        
    }
    
    
    func checkValidation()  {
        
        if txtFieldSearchArea.text == "Search area/locality"
        {
            Toast(Title: "", Text: "Please enter the area/locality", delay: 5)
        }
            
            
        else
        {
            lblRecentlyVisited.text = "Search Results"
            lblRecentlyVisited.textColor = colorWithHexString(hexString: "#57968B")
            recentlyVisitedView.backgroundColor = colorWithHexString(hexString: "#57968B")
            
            lblMap.isHidden = false
            lblMap.textColor = colorWithHexString(hexString: "#8d8585")
            
            //lblSerachVendors.isHidden = false
            mapSepratorView.isHidden = true
            recentlyVisitedView.isHidden = false
            
//            lblAppointmentHeight.constant = 0
//            lblAppointment.isHidden = true
            
            searchButtonClickedFlag = true
            tableViewSearchVendor.isHidden = true
            tableViewSearchResult.isHidden = false
            tableViewSearchResult .reloadData()
            
            get_Serach_Result()
        }
        
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: tableViewSearchResult) ?? false || (touch.view?.isDescendant(of: tableViewSearchVendor))! {
            
            // Don't let selections of auto-complete entries fire the
            // gesture recognizer
            return false
        }
        
        return true
    }
    
    @IBAction func btntopViewSearchClicked(_ sender: Any)
    
    {
        
       checkValidation()
        dismissKeyboard()
     tableViewSearchVendortop.constant = tableViewSearchVendortop.constant + 21
        
    }
    
    
    
}

