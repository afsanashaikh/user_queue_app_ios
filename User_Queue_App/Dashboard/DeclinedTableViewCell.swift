//
//  DeclinedTableViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 09/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class DeclinedTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var vendorImageView: UIImageView!
    
    
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblExpectedTime: UILabel!
    
    @IBOutlet weak var lblExpectedAt: UILabel!
    
    @IBOutlet weak var lblQueuePosition: UILabel!
    
    @IBOutlet weak var lblInTheQueue: UILabel!
    
    @IBOutlet weak var lblTotalNumberOfGuest: UILabel!
    
    
    @IBOutlet weak var lblTotalGuest: UILabel!
    
    @IBOutlet weak var lblQueue: UILabel!
    
    @IBOutlet weak var lblQueueTypw: UILabel!
    
    
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var lblVendorAddress: UILabel!
    
    @IBOutlet weak var messageImageView: UIImageView!
    
    @IBOutlet weak var lblFirst: UILabel!
    
    @IBOutlet weak var lblSecond: UILabel!
    
      @IBOutlet weak var lblTh: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        vendorImageView.layer.cornerRadius = 8
        vendorImageView.clipsToBounds = true
        vendorImageView.layer.masksToBounds = true
        
        mainView.layer.cornerRadius = 15
        mainView.clipsToBounds = true
        mainView.layer.masksToBounds = true
       // mainView.layer.borderWidth = 0.5
        
        lblStatus.layer.cornerRadius = 5
        lblStatus.clipsToBounds = true
        lblStatus.layer.masksToBounds = true
    }
  
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
