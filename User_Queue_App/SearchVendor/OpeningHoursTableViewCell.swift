//
//  OpeningHoursTableViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 17/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class OpeningHoursTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var lblDay: UILabel!
    
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblOpeningHours: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mainView.layer.cornerRadius = 15
        mainView.clipsToBounds = true
        mainView.layer.masksToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
