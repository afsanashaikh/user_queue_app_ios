//
//  RunningQueueViewController.swift
//  User_Queue_App
//
//  Created by Mac on 04/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class RunningQueueViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {

    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    
    @IBOutlet weak var topView: UIView!
    
    
    @IBOutlet weak var txtFieldServices: UITextField!
    
    @IBOutlet weak var lblQueueName: UILabel!
    
    @IBOutlet weak var txtViewDiscription: UITextView!
    
    
    @IBOutlet weak var lblTh: UILabel!
    
    
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblexpectedTime: UILabel!
    
    @IBOutlet weak var txtFieldNoOfPersons: UITextField!
    
    @IBOutlet weak var txtFieldMintsBefore: UILabel!
    
    @IBOutlet weak var lblAverageTime: UILabel!
    
    
    
    
    @IBOutlet weak var servicesTableView: UITableView!
    
    @IBOutlet weak var servicesTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var notifyMePopUpView: UIView!
    
    @IBOutlet weak var btnJoin: UIButton!
    
    
    @IBOutlet weak var lblQueuePosition: UILabel!
    
    @IBOutlet weak var btnCancel: UIButton!
  
    @IBOutlet weak var lblTodaysSchedule: UILabel!
    
    @IBOutlet weak var mintsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var noOfPersonsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var btnNotifyMeConfirm: UIButton!
    
    @IBOutlet weak var btnNotifyMeCancel: UIButton!
    
    
    var queueDetailsArray = NSMutableArray()
    var servicesArray =  NSArray()
    var notifyMasterArray =  NSArray()
    var slotsArray =  NSArray()
    var mintsArray = NSArray()
    var noOfPersonsArray :[String] = []
    
    var fromServiceFlag = false
    var defaultId:Int!
    var serviceId:Int!
    
    
    
    var mintSelectedIndexPath : Int!
    
    
    
    
    
    var noOfPersonsselectedIndexPath : Int!
    var venderId: Int!
    var slotArray: [Any] = []
    var notifyId :Any!
    var defaultNoOfPersons:Any!
    var currentDateStr :String!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnJoin.layer.cornerRadius = 10
        btnJoin.clipsToBounds = true
        btnJoin.layer.masksToBounds = true
        
        btnCancel.layer.cornerRadius = 10
        btnCancel.clipsToBounds = true
        btnCancel.layer.masksToBounds = true
        
        
        
        btnNotifyMeConfirm.layer.cornerRadius = 10
        btnNotifyMeConfirm.clipsToBounds = true
        btnNotifyMeConfirm.layer.masksToBounds = true
        
        btnNotifyMeCancel.layer.cornerRadius = 10
        btnNotifyMeCancel.clipsToBounds = true
        btnNotifyMeCancel.layer.masksToBounds = true
        
        backgroundView.isHidden = true
        notifyMePopUpView.isHidden = true
        
        loadArrays()
        get_Vender_QueueDetails()
        registerNibs()
        
        
        let currentDate = Date()
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM'-'dd'-'yyyy"
        
        currentDateStr = dateFormatter1.string(from: currentDate)
        
        var currentStrArr = currentDateStr.components(separatedBy: "-")
        var currentDate1 = currentStrArr[1]
        
        
        //Add Tap Gesture
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        scrollView.addGestureRecognizer(tap)
        
        txtFieldNoOfPersons.delegate = self
        
        
        createPickerView()
        dismissPickerView()
        
    }
   
    @objc func dismissKeyboard() {
      
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFieldNoOfPersons
        {
            view.endEditing(true)
            
            return true
        }
        return false
    }
    override func viewDidLayoutSubviews() {
        super .viewDidLayoutSubviews()
        topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 30.0)
        topView.backgroundColor = colorWithHexString(hexString: "#57968B")
        notifyMePopUpView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
    }
    
    func registerNibs(){
      
        
        
        mintsCollectionView.register(UINib(nibName: "NotifyMeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "Cell")
        
        self.mintsCollectionView.allowsSelection  = true;
        
        noOfPersonsCollectionView.register(UINib(nibName: "NotifyMeCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "Cell")
        
        self.noOfPersonsCollectionView.allowsSelection  = true;
        
        
        
        
    }
    
    
    @IBAction func btnDropDownClicked(_ sender: Any)
    {
        
//        servicesTableView.reloadData()
//        servicesTableViewHeight.constant = servicesTableView.contentSize.height
//
    }
    func createPickerView() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        txtFieldServices.inputView = pickerView
        //  scrollView.addSubview(pickerView)
        
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        // let button = UIBarButtonItem(title: “Done”, barButtonSystemItem: .plain, target: self, action: #selector(self.action))
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(self.action) )
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        txtFieldServices.inputAccessoryView = toolBar
    }
    @objc func action() {
        view.endEditing(true)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        print(servicesArray.count)
        return  servicesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print(servicesArray)
        let dict = servicesArray[row] as! NSDictionary
        let serviceName = dict.value(forKey: "ServiceName")as! NSString
        
        return serviceName as String
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dict = servicesArray[row] as! NSDictionary
        let serviceName = dict.value(forKey: "ServiceName")as! NSString
        txtFieldServices.text = serviceName as String
        
        
        self.serviceId = dict.value(forKey: "ID")as! Int
        
        fromServiceFlag = true
        servicesTableViewHeight.constant = 0
        
        get_Vender_QueueDetails()
        
    }
    func get_Vender_QueueDetails() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
        //  let userid = 12
     //   let venderId = 24
        if fromServiceFlag == true
        {
            Param["ServiceId"] = serviceId
        }
        else
        {
            Param["ServiceId"] = ""
        }
        Param["VendorQueueId"] = venderId
        //  Param["ServiceId"] = ""
        Param["DayId"] = ""
        Param["Date"] = ""
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetVendorQueueDetails)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetVendorQueueDetails+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSDictionary{
                            //  self.queueDetailsArray = data.mutableCopy() as! NSMutableArray
                            
                            self.lblQueueName.text = data.value(forKey: "QueueName")as? String
                            self.lblDescription.text = data.value(forKey: "ShortDesc")as? String
                            

                            let AverageTime = data.value(forKey: "AvgTime")
                            
                            if let AverageTime = AverageTime as? String {
                                // You have your String, process as needed
                                
                                print(AverageTime)
                                
                               self.lblAverageTime.text = AverageTime
                                
                                
                                
                            } else if let AverageTime = AverageTime as? NSNull {
                                // It was "null", process as needed
                                print("vendorAddress is null")
                                
                                self.lblAverageTime.text = ""
                                
                            } else {
                                // It is something else, possible nil, process as needed
                                
                                print(AverageTime)
                            }
                         
                            
                            let expectedTime = data.value(forKey: "ExpectedAt")
                            
                            if let expectedTime = expectedTime as? String {
                                // You have your String, process as needed
                                
                                print(expectedTime)
                                
                                self.lblexpectedTime.text = expectedTime
                                
                                
                                
                            } else if let expectedTime = expectedTime as? NSNull {
                                // It was "null", process as needed
                                print("vendorAddress is null")
                                
                                self.lblAverageTime.text = ""
                                
                            } else {
                                // It is something else, possible nil, process as needed
                                
                                print(expectedTime)
                            }
                            
                            let QueuePosition = data.value(forKey: "ExpNo")
                            
                            if var QueuePosition = QueuePosition as? Int {
                                // You have your String, process as needed
                                
                                print(QueuePosition)
                                
                                if QueuePosition > 11 && QueuePosition < 13
                                {
                                    self.lblTh.text = "th"
                                }
                                
                                else
                                {
                                    switch(QueuePosition % 10)
                                    {
                                    case 1:
                                        self.lblTh.text = "st"
                                        
                                    case 2:
                                        self.lblTh.text = "nd"
                                        break
                                    case 3:
                                        self.lblTh.text = "rd"
                                        break
                                        
                                       
                                        
                                    default:
                                        self.lblTh.text = "th"
                                        break
                                    }
                                }
                                
                                let queuePositionStr: String = "\(QueuePosition)"
                                
                                self.lblQueuePosition.text = queuePositionStr
                                
                            
                                
                            
                                
                            } else if let QueuePosition = QueuePosition as? NSNull {
                                // It was "null", process as needed
                                print("vendorAddress is null")
                                
                                self.lblQueuePosition.text = ""
                                
                            } else {
                                // It is something else, possible nil, process as needed
                                
                                print(QueuePosition)
                                self.lblQueuePosition.text = QueuePosition as! String
                            }
                            
                            
                            self.servicesArray = data.value(forKey: "Services") as! NSArray
                            
                            print(self.servicesArray)
                            
                            let dict = self.servicesArray[0] as! NSDictionary
                            
                            if self.fromServiceFlag == true
                            {
                                
                            }
                            else
                                
                            {
                                let dict = self.servicesArray[0] as! NSDictionary
                                
                                let ServiceName = dict.value(forKey: "ServiceName") as! String
                                
                                
                                
                                self.txtFieldServices.text = ServiceName
                            }
                            
                            
                            self.notifyMasterArray = data.value(forKey: "NotifyMaster") as! NSArray
                            
                            
//                            let QueuePosition = data.value(forKey: "ExpNo")
//
//                            if let QueuePosition = QueuePosition as? Int {
//                                // You have your String, process as needed
//
//                                print(QueuePosition)
//
//                                let queuePositionStr: String = "\(QueuePosition)"
//
//                                self.lblQueuePosition.text = queuePositionStr
//
//
//
//                            } else if let QueuePosition = QueuePosition as? NSNull {
//                                // It was "null", process as needed
//                                print("vendorAddress is null")
//
//                                self.lblQueuePosition.text = ""
//
//                            } else {
//                                // It is something else, possible nil, process as needed
//
//                                print(QueuePosition)
//                                self.lblQueuePosition.text = QueuePosition as! String
//                            }
//
                            
//                            self.slotsArray = data.value(forKey: "Slots") as! NSArray
//                           
//                            print(self.slotsArray)
//                            print(self.slotsArray.count)
                             self.defaultNoOfPersons = data.value(forKey: "DefaultPersonBefore")as! Int
                            
                            self.mintsCollectionView.reloadData()
                            
                            
                            self.defaultId = data.value(forKey: "DefaultNotiId")as! Int
                            for i in 0..<self.notifyMasterArray.count
                            {
                                let dict = self.notifyMasterArray[i] as! NSDictionary
                                
                                var ServiceId = dict.value(forKey: "Id") as! Int
                                
                                if ServiceId == self.defaultId
                                {
                                    //let ServiceIdStr: String = "\(ServiceId)"
                                    
                                    let servicename = dict.value(forKey: "Name") as! String
                                    
                                    self.txtFieldMintsBefore.text = servicename + " mints before"
                                }
                            }
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                    ActivityIndicator.stopIndicator()
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                ActivityIndicator.stopIndicator()
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    
    
    func join_Queue() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var joinQueueParam = [String: Any]()
        var Param = [String:Any]()
        
        var userjoinQueue = [String: Any]()
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
        //  let userid = 12
        // let venderId = 24
        if fromServiceFlag == true
        {
            joinQueueParam["ServiceId"] = serviceId
        }
        else
        {
            joinQueueParam["ServiceId"] = ""
        }
      //  joinQueueParam["VendorQueueId"] = venderId
        
        
        
        let userid = UserDefaults.standard.value(forKey: "UserId")
        
        joinQueueParam["AppoDateTime"] = currentDateStr
        joinQueueParam["VendorQueueId"] = venderId
        joinQueueParam["UserId"] = userid
        // Param["ServiceId"] = serviceId
        
        
        
        Param["UserJoinQueue"] = joinQueueParam
        
        Param["SlotId"] = slotArray
        Param["NotiId"] = notifyId
        Param["PersonBefore"] = defaultNoOfPersons
        Param["PersonWithMe"] = txtFieldNoOfPersons.text
        print("Param is \(Param)")
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+UserJoinQueue)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+UserJoinQueue+"?" )! , method: .post, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        
                        let messageStr = dictionary["Message"] as? String
                        self.Toast(Title: "", Text: messageStr! , delay: 5)
                        //Message
                        
                       
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            //print(dictionary["Message"]as! String)
                            let messageStr = dictionary["Message"] as? String
                            self.Toast(Title: "", Text: messageStr! , delay: 5)
                            
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                    ActivityIndicator.stopIndicator()
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                self.Toast(Title: "", Text: "Server not responding please try again later.", delay: 5)
                
                ActivityIndicator.stopIndicator()
                ActivityIndicator.stopIndicator()
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
                
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(objVc, animated: true)
                
                break
            default:
                break
            }
        }
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
//        
//        self.present(objVc, animated: true, completion: nil)
    }
    
    func Toast(Title:String ,Text:String, delay:Int) -> Void {
        let alert = UIAlertController(title: Title, message: Text, preferredStyle: .alert)
        self.present(alert, animated: true)
        let deadlineTime = DispatchTime.now() + .seconds(delay)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
        
       
        
    }
    
    @IBAction func btnNotifyMeClicked(_ sender: Any)
    {
        
        backgroundView.isHidden = false
        notifyMePopUpView.isHidden = false
    }
    
    func loadArrays() {
        
        //For Days collection View
        
       
        noOfPersonsArray = ["1","2","3","4","5","6"]
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath:  IndexPath) -> UICollectionViewCell {
        
        if collectionView == mintsCollectionView {
            
            let cell : NotifyMeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! NotifyMeCollectionViewCell
            
            let dict = notifyMasterArray[indexPath.item]as! NSDictionary
            
            let dayStr = dict.value(forKey: "Name")as! String
            cell.secondLbl.text = dayStr
            
            if mintSelectedIndexPath == indexPath.item
            {
                
                let ServiceId = dict.value(forKey: "Id") as! Int
                
                if ServiceId == self.defaultId
                {
                    cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
                    cell.innerView.backgroundColor = colorWithHexString(hexString: "#4EA191")
                    //cell.firstLbl.textColor = UIColor .white
                    cell.secondLbl.textColor = UIColor .white
                }
                else
                {
                    cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
                    cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
                    //cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
                    cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
                }
                
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#4EA191")
                //cell.firstLbl.textColor = UIColor .white
                cell.secondLbl.textColor = UIColor .white
                
                
                let dict = self.notifyMasterArray[mintSelectedIndexPath] as! NSDictionary
                
                notifyId = dict.value(forKey: "Id")
                
                
                
                //     var gradientView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 35))
                /*  let gradientLayer:CAGradientLayer = CAGradientLayer()
                 gradientLayer.frame.size = cell.innerView.frame.size
                 gradientLayer.colors =
                 [UIColor.white.cgColor,UIColor.red.withAlphaComponent(1).cgColor]
                 //Use diffrent colors
                 cell.outerView.layer.addSublayer(gradientLayer)*/
                
                
                
                
            }
            else
            {
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
                //cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
                cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
                
                /*  let gradientLayer:CAGradientLayer = CAGradientLayer()
                 gradientLayer.frame.size = cell.innerView.frame.size
                 gradientLayer.colors =
                 [UIColor.lightGray.cgColor,UIColor.lightGray.withAlphaComponent(1).cgColor]
                 //Use diffrent colors
                 cell.outerView.layer.addSublayer(gradientLayer)*/
                notifyId = defaultId
            }
            
            
            return cell
            
        }
        else   {
            
            let cell : NotifyMeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! NotifyMeCollectionViewCell
            
            let dayStr = noOfPersonsArray[indexPath.item]
            cell.secondLbl.text = dayStr
            
            if noOfPersonsselectedIndexPath == indexPath.item
            {
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#57968B")
                //cell.firstLbl.textColor = UIColor .white
                cell.secondLbl.textColor = UIColor .white
                
                
            }
            else
            {
                cell.innerView.backgroundColor = colorWithHexString(hexString: "#EBEAEF")
                cell.outerView.backgroundColor = colorWithHexString(hexString: "#FFFFFF")//696C77
                //cell.firstLbl.textColor = colorWithHexString(hexString: "#696C77")
                cell.secondLbl.textColor = colorWithHexString(hexString: "#696C77")
                
            }
            return cell
            
        }
       
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == mintsCollectionView
        {
            return notifyMasterArray.count
        }
        else 
        {
            return noOfPersonsArray.count
        }
            
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == mintsCollectionView
        {
            
            mintSelectedIndexPath = indexPath.item
            
            mintsCollectionView.reloadData()
            
        }
        else
        {
            
            noOfPersonsselectedIndexPath = indexPath.item
            
            noOfPersonsCollectionView.reloadData()
            
        }
        
    }
    
    //TableView Delegate
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return servicesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        servicesTableView.register(UINib(nibName: "RecentlySearcgedAreaCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecentlySearcgedAreaCell
        
        cell1.selectionStyle = .none
        
        print(servicesArray.count)
        let dict = servicesArray[indexPath.row] as! NSDictionary
        
        let ServiceName = dict.value(forKey: "ServiceName") as! String
        
        
        cell1.lblAreaName.text = ServiceName
        cell1.serachImgView.isHidden = true
        
        return cell1
    }
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath)
    {
        let dict = servicesArray[indexPath.row] as! NSDictionary
        
        let ServiceName = dict.value(forKey: "ServiceName") as! String
        
        self.serviceId = dict.value(forKey: "ID")as! Int
        
        txtFieldServices.text = ServiceName
        
        fromServiceFlag = true
        servicesTableViewHeight.constant = 0
        
        get_Vender_QueueDetails()
        
    }
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    
   
    /*
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNotifyMeCancelClicked(_ sender: Any)
    {
        backgroundView.isHidden = true
        notifyMePopUpView.isHidden = true
        
    }
    @IBAction func btnnotifyMeConfirmedClicked(_ sender: Any) {
        var refreshAlert = UIAlertController(title: "Alert", message: "Are you sure you want to update?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
            
            self.present(objVc, animated: true, completion: nil)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            
            self.backgroundView.isHidden = true
            self.notifyMePopUpView.isHidden = true
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnCancelClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnJoinClikced(_ sender: Any)
    {
        
        join_Queue()
    }
}

