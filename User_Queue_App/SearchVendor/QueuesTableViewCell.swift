//
//  QueuesTableViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 17/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class QueuesTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var vendorName: UILabel!
    
    @IBOutlet weak var lblVendorDesignation: UILabel!
    
    
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblExpectedTime: UILabel!
    
    @IBOutlet weak var lblInTheQueue: UILabel!
    
    @IBOutlet weak var lblFirst: UILabel!
    
    
    @IBOutlet weak var lblQueuePosition: UILabel!
    
    @IBOutlet weak var lblAvgTime: UILabel!
    
    
    @IBOutlet weak var thLabel: UILabel!
    
    
    @IBOutlet weak var nextAvailableSlotView: UIView!
    
    @IBOutlet weak var lblNextAvailableSlots: UILabel!
    
    @IBOutlet weak var lblSlotTiming: UILabel!
    
    
    @IBOutlet weak var nextAvialableSlotViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var todaysScheduleView: UIView!
    
    
    @IBOutlet weak var todaysScheduleViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblTodaysSchedule: UILabel!
    
    @IBOutlet weak var lblSchedule: UILabel!
    
    
    @IBOutlet weak var reasonView: UIView!
    
    
    @IBOutlet weak var reasonsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblReason: UILabel!
    
    
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var view2Height: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblSlash: UILabel!
    @IBOutlet weak var lblScheduleHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        mainView.layer.cornerRadius = 15
        mainView.clipsToBounds = true
        mainView.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
