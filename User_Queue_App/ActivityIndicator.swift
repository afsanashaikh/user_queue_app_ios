//
//  ActivityIndicator.swift
//  CraziApp
//
//  Created by SILICON on 04/02/19.
//  Copyright © 2019 SILICON. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class ActivityIndicator: NSObject {

    static let activityIndicatorView =  NVActivityIndicatorView.init(frame: CGRect.init(x: UIScreen.main.bounds.midX - 25 , y: UIScreen.main.bounds.midY, width: 50, height: 50), type: .circleStrokeSpin, color: .blue, padding: 0)
    
    
    class func startIndicator(view: UIView){
        
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    class func stopIndicator(){
        activityIndicatorView.removeFromSuperview()
        activityIndicatorView.stopAnimating()
    }
    
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    
}
