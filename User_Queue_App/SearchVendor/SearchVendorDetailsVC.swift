//
//  SearchVendorDetailsVC.swift
//  User_Queue_App
//
//  Created by Mac on 17/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class SearchVendorDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource ,CLLocationManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var lblQueues: UILabel!
    
    @IBOutlet weak var queuesView: UIView!
    
    @IBOutlet weak var lblServices: UILabel!
    
    
    @IBOutlet weak var servicesView: UIView!
    
    
    @IBOutlet weak var lblOpeningHours: UILabel!
    
    @IBOutlet weak var openingHoursView: UIView!
    
    
    @IBOutlet weak var listTableView: UITableView!
    
    @IBOutlet weak var tableViewServices: UITableView!
    
    
    @IBOutlet weak var googleMapView: GMSMapView!
    
   
    @IBOutlet weak var vendorImageView: UIImageView!
    
    
    @IBOutlet weak var vendorName: UILabel!
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var vendorAddress: UILabel!
    
    
    var queuesFlag = false
    var servicesFlag = false
    var openingHoursFlag = false
    var mapFlag = false
    
    
    var daysArray: [[String:String]] = [[ : ]]
    var serviceArray:[String] = []
    
    var locationManager = CLLocationManager()
    var doublelat = Double()
    var doublelon = Double()
    var businessName: String!
    var optionsArray: [String] = []
    var selectedIndexPath : Int!
    
    @IBOutlet weak var openingHoursTableView: UITableView!
    
    
    @IBOutlet weak var lblMap: UILabel!
    
    @IBOutlet weak var mapView: UIView!
   
     var venderId: Int!
    
    var servicesArray = NSArray()
    var queueInfoArray = NSArray()
    var openingHoursinfoArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       
        
    
        
        
        topView.backgroundColor = colorWithHexString(hexString: "#57968B")
        topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 30.0)
      
        registerNibs()
        tableViewServices.isHidden = true
        openingHoursTableView.isHidden = true
        listTableView.isHidden = false
        googleMapView.isHidden = true
        loadArrays()
        
        get_Vendor_Info()

        get_Queuue_Details()
        
        
        
        locationManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.startUpdatingLocation()
        
        locationManager.startMonitoringSignificantLocationChanges()
        
        // Here you can check whether you have allowed the permission or not.
        
        if CLLocationManager.locationServicesEnabled()
        {
            switch(CLLocationManager.authorizationStatus())
            {
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("Authorize.")
                
                break
                
            case .notDetermined:
                
                print("Not determined.")
                
                break
                
            case .restricted:
                
                print("Restricted.")
                
                break
                
            case .denied:
                
                print("Denied.")
            }
        }
      
        
        
        vendorImageView.layer.cornerRadius = 8
        vendorImageView.clipsToBounds = true
        vendorImageView.layer.masksToBounds = true
        
        optionsArray = ["Queues","Services","Opening Hours","Map"]
        
        selectedIndexPath = 0
    }
    
   /* override func viewDidLayoutSubviews() {
        super .viewDidLayoutSubviews()
        topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 30.0)
     //   topView.backgroundColor = colorWithHexString(hexString: "#57968B")
    }*/
    
    func get_Vendor_Info() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
        // let userid = 12
        
    //    venderId = 108
        
        Param["VendorId"] = self.venderId
        
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetVendorsDetails)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetVendorsDetails+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSDictionary{
                            
                            print(data)
                            print(data["ServiceInfo"])
                            print(data["QueueInfo"])
                            
//                            self.vendorName.text = data["BusinessName"] as! String
//                            self.vendorAddress.text = data["BusinessAdress"] as! String
                            
                            
                            let venderName = data["BusinessName"]
                            
                            if let venderName = venderName as? String {
                                // You have your String, process as needed
                                
                                print(venderName)
                                
                                self.vendorName.text = venderName as! String
                                self.businessName = venderName
                                
                                
                            } else if let venderName = venderName as? NSNull {
                                // It was "null", process as needed
                                print("vendorAddress is null")
                                
                                self.vendorName.text = ""
                                 self.businessName = ""
                                
                            } else {
                                // It is something else, possible nil, process as needed
                                
                                print(venderName)
                            }
                            
                            
                            let vendorAddress = data["BusinessAdress"]
                            
                            if let vendorAddress = vendorAddress as? String {
                                // You have your String, process as needed
                                
                              //  print(vendorAddress)
                                
                                 self.vendorAddress.text = vendorAddress as! String
                                
                                
                                
                            } else if let vendorAddress = vendorAddress as? NSNull {
                                // It was "null", process as needed
                                print("vendorAddress is null")
                                
                                 self.vendorAddress.text = ""
                                
                            } else {
                                // It is something else, possible nil, process as needed
                                
                                print(vendorAddress)
                            }
                            
                            self.doublelat = data.value(forKey: "Latitude") as! Double
                            self.doublelon = data.value(forKey: "Longitude")as! Double
                            
                            
                           
                            var profilePicUrl =  data["ProfilePic"]
                            
                            var profilePicUrlStr :String!
                            if let profilePicUrl = profilePicUrl as? String {
                                // You have your String, process as needed
                                
                                print(profilePicUrl)
                                
                                profilePicUrlStr = baseUrl + profilePicUrl
                                self.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                                
                                
                                
                                
                                //
                                
                            } else if let profilePicUrl = profilePicUrl as? NSNull {
                                // It was "null", process as needed
                                print("vendorAddress is null")
                                
                                profilePicUrlStr = ""
                                
                        self.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                                
                                
                            } else {
                                // It is something else, possible nil, process as needed
                                
                                
                                profilePicUrlStr = ""
                                
                            self.vendorImageView.sd_setImage(with: URL(string:profilePicUrlStr), placeholderImage: UIImage(named: "default_image"))
                                
                                
                            }
                        
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    
    func get_Queuue_Details() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
        // let userid = 12
        
        //    venderId = 108
        
        Param["VendorId"] = self.venderId
        
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetVendorsQueueDetails)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetVendorsQueueDetails+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response
            {
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSArray{
                            
                            self.queueInfoArray = data.mutableCopy() as! NSArray
                            
                            self.listTableView.reloadData()
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    
    
    
    func get_service_Details() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
        // let userid = 12
        
        //    venderId = 108
        
        Param["VendorId"] = self.venderId
        
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetVendorsServiceDetails)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetVendorsServiceDetails+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSArray{
                            
                            self.servicesArray = data.mutableCopy() as! NSArray
                            
                            self.tableViewServices.reloadData()
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    func get_OpeningHours_Details() {
        
        ActivityIndicator.startIndicator(view: self.view)
        
        
        
        
        var Param = [String: Any]()
        
        
        
        
        //let userid = UserDefaults.standard.value(forKey: "UserId")
        
        // let userid = 12
        
        //    venderId = 108
        
        Param["VendorId"] = self.venderId
        
        
        
        print("Param is \(Param)")
        print("URL is \(baseUrl+GetVendorsOpeningHourDetails)")
        
        Webservices.dataTask_POST(Foundation.URL(string:baseUrl+GetVendorsOpeningHourDetails+"?" )! , method: .get, param: Param) { (response) in
            
            print(response)
            switch response{
                
                
            case .success(let dictionary as [String: Any]):
                ActivityIndicator.stopIndicator()
                print(dictionary)
                print(response)
                print(dictionary["Response"])
                let status = dictionary["Status"] as? Bool
                print(status)
                
                
                let responseStr = dictionary["Response"] as? String
                
                if status != nil {
                    ActivityIndicator.stopIndicator()
                    if status == true
                    {
                        
                        if let data =  dictionary["Response"] as? NSArray{
                            
                            self.openingHoursinfoArray = data.mutableCopy() as! NSArray
                            print(self.openingHoursinfoArray)
                            self.openingHoursTableView.reloadData()
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        DispatchQueue.main.async {
                            //   Alert.ShowAlert(title: "", message: (dictionary["message"] as! String), viewcontroller: self)
                            print(dictionary["Message"]as! String)
                            ActivityIndicator.stopIndicator()
                        }
                    }
                    
                }
                else
                {
                    // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                    ActivityIndicator.stopIndicator()
                    
                }
                
                break
            case .failure( _):
                // Alert.ShowAlert(title: "", message: "Server not responding please try again later.", viewcontroller: self)
                ActivityIndicator.stopIndicator()
                
                break
            default:
                break
            }
        }
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {

        self.navigationController?.popViewController(animated: true)
        
    }
    
    @objc func queuelabelTapped()
    {
        lblQueues.textColor = colorWithHexString(hexString: "#57968B")
        queuesView.backgroundColor = colorWithHexString(hexString: "#57968B")
        
        lblServices.textColor = UIColor .lightGray
        servicesView.backgroundColor = UIColor.lightGray
        
        lblOpeningHours.textColor = UIColor .lightGray
        openingHoursView.backgroundColor = UIColor.lightGray
        
        lblMap.textColor = UIColor .lightGray
        mapView.backgroundColor = UIColor.lightGray

        queuesFlag = true
        servicesFlag = false
        openingHoursFlag = false
        mapFlag = false
        
        listTableView .reloadData()
        listTableView .isHidden = false
        tableViewServices .isHidden = true
        openingHoursTableView.isHidden = true
        googleMapView.isHidden = true

        
    }
    @objc func servicelabelTapped()
    {
        topView.backgroundColor = colorWithHexString(hexString: "#617189")

        lblServices.textColor = colorWithHexString(hexString: "#57968B")
        servicesView.backgroundColor = colorWithHexString(hexString: "#57968B")
        
        lblQueues.textColor = UIColor .lightGray
        queuesView.backgroundColor = UIColor.lightGray
        
        lblOpeningHours.textColor = UIColor .lightGray
        openingHoursView.backgroundColor = UIColor.lightGray
        
        lblMap.textColor = UIColor .lightGray
        mapView.backgroundColor = UIColor.lightGray
        
        servicesFlag = true
        queuesFlag = false
        openingHoursFlag = false
        mapFlag = false
        
       // listTableView .reloadData()
        
        listTableView .isHidden = true
        tableViewServices .isHidden = false
        openingHoursTableView.isHidden = true
        googleMapView.isHidden = true

        
        get_service_Details()
        
    }
    
    @objc func openingHourslabelTapped()
    {
        
        topView.backgroundColor = colorWithHexString(hexString: "#617189")
        
        lblOpeningHours.textColor = colorWithHexString(hexString: "#57968B")
        openingHoursView.backgroundColor = colorWithHexString(hexString: "#57968B")
        
        lblServices.textColor = UIColor .lightGray
        servicesView.backgroundColor = UIColor.lightGray
        
        lblQueues.textColor = UIColor .lightGray
        queuesView.backgroundColor = UIColor.lightGray
        
        lblMap.textColor = UIColor .lightGray
        mapView.backgroundColor = UIColor.lightGray
        
        
        openingHoursFlag = true
        servicesFlag = false
        queuesFlag = false
        mapFlag = false
        
        listTableView .reloadData()
        
        listTableView.isHidden = true
        tableViewServices.isHidden = true
        openingHoursTableView.isHidden = false
        googleMapView.isHidden = true
        //openingHoursTableView.reloadData()
        
        get_OpeningHours_Details()
        
    }
   
    
    @objc func maplabelTapped()
    {
        lblOpeningHours.textColor = UIColor .lightGray
        openingHoursView.backgroundColor = UIColor.lightGray
        
        lblMap.textColor = colorWithHexString(hexString: "#57968B")
        mapView.backgroundColor = colorWithHexString(hexString: "#57968B")
        
        lblServices.textColor = UIColor .lightGray
        servicesView.backgroundColor = UIColor.lightGray
        
        lblQueues.textColor = UIColor .lightGray
        queuesView.backgroundColor = UIColor.lightGray
        
        openingHoursFlag = false
        servicesFlag = false
        queuesFlag = false
        mapFlag = true
         topView.backgroundColor = colorWithHexString(hexString: "#57968B")
        listTableView .reloadData()
        
        listTableView.isHidden = true
        tableViewServices.isHidden = true
        openingHoursTableView.isHidden = false
        
        googleMapView.isHidden = false
       

        showMarker()
        
    }
    func showMarker()
    {
        
            //possitioning marker,here mark,mrk,name are mutable arrays
            
            
            
            let position = CLLocationCoordinate2D(latitude: self.doublelat, longitude: self.doublelon)
            
            if (position.latitude > 0  && position.longitude > 0)
            {
                let marker = GMSMarker()
                let camera = GMSCameraPosition.camera(withLatitude: self.doublelat,longitude: self.doublelon,zoom: 6)
                // let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
                
                
                print(camera)
                print(camera)
                
                // [self.mapView animateToCameraPosition:cameraPosition];
                
                self.googleMapView .camera = camera
                
                self.googleMapView .animate(to: camera)
                
                marker.position = position
                marker.title = self.businessName
                marker.map = self.googleMapView
            }
                
            else
            {
               let camera = GMSCameraPosition.camera(withLatitude: self.doublelat,longitude: self.doublelon,zoom: 6)
                // let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
                
                
                
                self.googleMapView .camera = camera
            }
            
        
        
    }
    func loadArrays() {
        
        //For Days collection View
        
        daysArray = [
            [
                "date": "07 May",
                "day": "Tuesday",
                "sevice": "9:00-17:00",
                ],
            [
                "date": "08 May",
                "day": "Wedensday",
                "sevice": "9:00-12:00 | 13:00-17:00",
                ],
            [
                "date": "09 May",
                "day": "Thursday",
                "sevice": "9:00-17:00",
                ],
            [
                "date": "10 May",
                "day": "Friday",
                "sevice": "9:00-17:00",
                ],
            [
                "date": "11 May",
                "day": "Saturday",
                "sevice": "9:00-17:00 | 13:00-17:00",
                ],
            [
                "date": "12 May",
                "day": "Sunday",
                "sevice": "Non Working day",
                ],
            [
                "date": "13 May",
                "day": "Monday",
                "sevice": "9:00-17:00",
                ]
        ]
        
        serviceArray = ["Service 1","Service 2","Service 3","Service 4"]
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView {
       
        if tableView == openingHoursTableView
        {
            if openingHoursinfoArray.count > 0
            {
                let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
                label.textColor = UIColor .gray
                // label.font = UIFont(name: UIFont.fontNames(forFamilyName:"Sen-Regular")[0], size: 14.0)
                label.text = "Week's Schedule"
                return label
            }
            
           else
            {
                let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
                label.textColor = UIColor .gray
                // label.font = UIFont(name: UIFont.fontNames(forFamilyName:"Sen-Regular")[0], size: 14.0)
                label.text = "No data found"
                return label
            }
        }
        else
        {
            let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
            label.textColor = UIColor.red
            label.text = ""
            return label
        }
      
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tableViewServices
        {
           return servicesArray.count
        }
        else if tableView == openingHoursTableView
        {
            return openingHoursinfoArray.count
        }
        else
        {
           return queueInfoArray.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if (tableView == tableViewServices)
        {
            
            
           // tableViewServices.register(UINib(nibName: "QueuesTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let nib = UINib(nibName: "ServicesTableViewCell", bundle: nil)
            tableViewServices.register(nib, forCellReuseIdentifier: "ServicesTableViewCell")
            
           // listTableView.register(UINib(nibName: "QueuesTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "ServicesTableViewCell", for: indexPath) as! ServicesTableViewCell
           
            
            let dict = self.servicesArray[indexPath.row] as! NSDictionary
            
            let serviceName = dict.value(forKey: "ServiceName")
            
            if let serviceName = serviceName as? String {
                // You have your String, process as needed
                
                print(serviceName)
                
                cell1.lblService.text = serviceName as! String
                
                
                
            } else if let vendorName = serviceName as? NSNull {
                // It was "null", process as needed
              //  print("vendorAddress is null")
                
                cell1.lblService.text = ""
                
            } else {
                // It is something else, possible nil, process as needed
                
                print(serviceName)
            }
            
            
            
            let duration = dict.value(forKey: "ServiceName")
            
            if let duration = duration as? NSDictionary {
                // You have your String, process as needed
                
                print(serviceName)
                
               
                let duration = dict.value(forKey: "Duration") as! NSDictionary
                
                let mints = duration.value(forKey: "TotalMinutes") as! Int
                
                let mintsStr: String = "\(mints)"
                
                
                cell1.lblmints.text = mintsStr
                
                
                
            } else if let duration = duration as? NSNull {
                // It was "null", process as needed
                //  print("vendorAddress is null")
                
                cell1.lblmints.text = ""
                
            } else {
                // It is something else, possible nil, process as needed
                
                print(serviceName)
            }
            
        
           
            return cell1
            
            
        }
            
      else  if (tableView == openingHoursTableView)
        {
            
            
            // tableViewServices.register(UINib(nibName: "QueuesTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let nib = UINib(nibName: "OpeningHoursTableViewCell", bundle: nil)
            openingHoursTableView.register(nib, forCellReuseIdentifier: "OpeningHoursTableViewCell")
            
            // listTableView.register(UINib(nibName: "QueuesTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "OpeningHoursTableViewCell", for: indexPath) as! OpeningHoursTableViewCell
            
            
            //   cell1.contentView.backgroundColor = colorWithHexString(hexString: "#57968B")
            
            
            let dict = self.openingHoursinfoArray[indexPath.row] as! NSDictionary
            
            
            print(dict)
            
          //  let serviceName = dict.value(forKey: "ServiceName")
            
            
            
            
            let dateStr = dict.value(forKey: "DateStr") as! String
            cell1.lblDate.text = dateStr
            
            
            let dayDict = dict.value(forKey: "DayMaster") as! NSDictionary
            
            cell1.lblDay.text = dayDict.value(forKey: "DayName") as! String
            
            let OpeningHoursDict = dict.value(forKey: "OpenTime") as! NSDictionary
            
            let closingHoursDict = dict.value(forKey: "CloseTime") as! NSDictionary
            
            let openingHours = OpeningHoursDict.value(forKey: "Hours") as! Int
            let openingHoursStr: String = "\(openingHours)"

            
            let openingmints = OpeningHoursDict.value(forKey: "Minutes") as! Int
            let openingmintsStr: String = "\(openingmints)"

            
            let openingHoursMint = openingHoursStr + ":" + openingmintsStr
            
            
            let closingHours = closingHoursDict.value(forKey: "Hours") as! Int
            let closingHoursStr: String = "\(closingHours)"

            
            
            let closingmints = closingHoursDict.value(forKey: "Minutes") as! Int
            let closingmintsStr: String = "\(closingmints)"

            
            let closingHoursMint = closingHoursStr + ":" + closingmintsStr
            
            
          
            
            
                    let holidayFlagStr = dict.value(forKey: "IsHoliday") as! Bool
                    if holidayFlagStr == true
                    {
                        cell1.lblOpeningHours.text = "Non working day"
                        cell1.lblOpeningHours.textColor = UIColor.red
            }
            else
            
                    {
                        let opentimeStr = dict.value(forKey: "OpenTimeStr")as! String
                        let closetimeStr = dict.value(forKey: "CloseTimeStr")as! String
                        
                        
                        
                        let finaltime = opentimeStr + "-" + closetimeStr
                        cell1.lblOpeningHours.text = finaltime
                         cell1.lblOpeningHours.textColor = UIColor.black
            }
            
//            let dayStr = daysArray[indexPath.item]["day"]
//            cell1.lblDay.text = dayStr
//            
//            let serviceStr = daysArray[indexPath.item]["sevice"]
//            cell1.lblOpeningHours.text = serviceStr
//            
//            if dayStr == "Sunday"
//            {
//                cell1.lblOpeningHours.textColor = UIColor.red
//            }
//            else
//            {
//                cell1.lblOpeningHours.textColor = UIColor.black
//                
//            }
            
            return cell1
            
            
        }
            
            
        else
        {
            let nib = UINib(nibName: "QueuesTableViewCell", bundle: nil)
            listTableView.register(nib, forCellReuseIdentifier: "QueuesTableViewCell")
            
        
            
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "QueuesTableViewCell", for: indexPath) as! QueuesTableViewCell
            
          
            let dict = queueInfoArray[indexPath.row] as! NSDictionary
            print(dict)
            
            let queueType = dict.value(forKey: "QueueType") as! Int
            
      let vendorQueueDict = dict.value(forKey: "VendorQueue")as! NSDictionary
            
        cell1.vendorName.text = vendorQueueDict.value(forKey: "QueueName")as! String
        cell1.lblVendorDesignation.text = vendorQueueDict.value(forKey: "ShortDesc")as! String
        cell1.reasonView.isHidden = true
        cell1.reasonsViewHeight.constant = 0
         cell1.selectionStyle = .none
            
            if queueType == 1
            {
                let queueCount = dict.value(forKey: "InQueueCount")as! Int
                let queueCountStr: String = "\(queueCount)"
                
                
                cell1.lblExpectedTime.text = queueCountStr
                cell1.lblInTheQueue.text = "Slots booked"
                
               // let image: UIImage =
                cell1.imgView.image = UIImage(named: "icn_slotted_q_sm")
                
                
                
                cell1.lblExpectedTime.text = queueCountStr
                cell1.lblInTheQueue.text = "Slot Booked"
                
                
                
                
                
                
                let avgTimeDict = dict.value(forKey: "AvgTime")
                
                if let avgTimeDict = avgTimeDict as? NSDictionary {
                    // You have your String, process as needed
                    
                    print(avgTimeDict)
                    
                    
                    let avgTimeDict = dict.value(forKey: "AvgTime")as! NSDictionary
                    
                    let openingHours = avgTimeDict.value(forKey: "Hours") as! Int
                    let openingHoursStr: String = "\(openingHours)"
                    
                    
                    let openingmints = avgTimeDict.value(forKey: "Minutes") as! Int
                    let openingmintsStr: String = "\(openingmints)"
                    
                    
                    let openingHoursMint = openingHoursStr + ":" + openingmintsStr
                    
                    cell1.lblQueuePosition.text = openingHoursMint + "min"
                    
                    
                    
                } else if let avgTimeDict = avgTimeDict as? NSNull
                {
                    
                    
                    cell1.lblQueuePosition.text = ""
                    
                    
                } else {
                    // It is something else, possible nil, process as needed
                    
                    print(avgTimeDict)
                }
                
                
                
               // cell1.lblSchedule.text = dict.value(forKey: "TodaysSchedule")as! String
                
                let todaysSchedule = dict.value(forKey: "TodaysSchedule")
                
                if let todaysSchedule = todaysSchedule as? String {
                    // You have your String, process as needed
                    
                    print(todaysSchedule)
                    
                    cell1.lblSchedule.text = todaysSchedule as! String
//                    cell1.lblScheduleHeight.constant = cell1.lblSchedule.height()
//                    print(cell1.lblScheduleHeight.constant)
                    
                    
                } else if let todaysSchedule = todaysSchedule as? NSNull {
                    // It was "null", process as needed
                    print("vendorAddress is null")
                    
                    cell1.lblSchedule.text = ""
                    
                } else {
                    // It is something else, possible nil, process as needed
                    
                    print(todaysSchedule)
                }
                
                cell1.thLabel.isHidden = false   //TotalCount
                let totalCount = dict.value(forKey: "TotalCount")as! Int
                let totalCountStr: String = "\(totalCount)"
                cell1.thLabel.text =  totalCountStr
                cell1.lblSlash.isHidden = false
                
                cell1.lblExpectedTime.text = queueCountStr
                cell1.lblNextAvailableSlots.isHidden = true
                cell1.nextAvailableSlotView.isHidden = true
                cell1.nextAvialableSlotViewHeight.constant = 0
                
            }
            else
            {
             
                let queueCount = dict.value(forKey: "InQueueCount")as! Int
                let queueCountStr: String = "\(queueCount)"
                
                
                cell1.lblExpectedTime.text = queueCountStr
                cell1.lblInTheQueue.text = "In the Queue"
                
           //     let image: UIImage = !
                cell1.imgView.image = UIImage(named: "icn_running_q_sm")
                
                
                
                
                
                cell1.lblExpectedTime.text = queueCountStr
                cell1.lblInTheQueue.text = "In the Queue"
                
                
               
                
                
                let todaysSchedule = dict.value(forKey: "TodaysSchedule")
                
                if let todaysSchedule = todaysSchedule as? String {
                    // You have your String, process as needed
                    
                    print(todaysSchedule)
                    
                    cell1.lblSchedule.text = todaysSchedule as! String
                   // cell1.lblScheduleHeight.constant = cell1.lblSchedule.height()
                 //   print(cell1.lblScheduleHeight.constant)
                    
                } else if let todaysSchedule = todaysSchedule as? NSNull {
                    // It was "null", process as needed
                    print("vendorAddress is null")
                    
                    cell1.lblSchedule.text = ""
                    
                } else {
                    // It is something else, possible nil, process as needed
                    
                    print(todaysSchedule)
                }
                
                
                let nextSlot = dict.value(forKey: "NextSlot")
                
                if let nextSlot = nextSlot as? String {
                    // You have your String, process as needed
                    
                    print(nextSlot)
                    
                    cell1.lblSlotTiming.text = nextSlot as! String
                    
                    
                    
                } else if let nextSlot = nextSlot as? NSNull {
                    // It was "null", process as needed
                    print("vendorAddress is null")
                    
                    cell1.lblSlotTiming.text = ""
                    
                } else {
                    // It is something else, possible nil, process as needed
                    
                    print(todaysSchedule)
                }
                
                
                cell1.thLabel.isHidden = true
                cell1.lblNextAvailableSlots.isHidden = false
                cell1.nextAvailableSlotView.isHidden = false
                cell1.nextAvialableSlotViewHeight.constant = 30
               // cell1.lblSlotTiming.text = dict.value(forKey: "NextSlot")as! String
                 cell1.lblSlash.isHidden = true
            }
            
            return cell1
            
            
        }
       
    }
    func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath)
    {
        
        /* let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let objVc:SearchVendorDetailsVC =
         storyboard?.instantiateViewController(withIdentifier: "SearchVendorDetailsVC") as!SearchVendorDetailsVC
         self.present(objVc, animated: true, completion: nil)*/
        
        
        if tableView == listTableView
        {
            
            let dict = queueInfoArray[indexPath.row] as! NSDictionary
            print(dict)
            
            let queueType = dict.value(forKey: "QueueType") as! Int
            let venderQuueDict = dict.value(forKey: "VendorQueue")as! NSDictionary
             let vendorId = venderQuueDict.value(forKey: "Id") as! Int
           
            if queueType == 1
            {
               
               
                
                let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let objVc:VendorDetailsViewController =
                    storyboard?.instantiateViewController(withIdentifier: "VendorDetailsViewController") as!
                VendorDetailsViewController
               objVc.venderId = vendorId
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(objVc, animated: true)
            }
            
            
        else
        {
            
            
            let stroyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let objVc:RunningQueueViewController =
                storyboard?.instantiateViewController(withIdentifier: "RunningQueueViewController") as!
            RunningQueueViewController
            objVc.venderId = vendorId
            self.navigationController?.isNavigationBarHidden = true
            self.navigationController?.pushViewController(objVc, animated: true)
        }
    }
    }
    //Collectionview delegates
    
    
    
    
   
func registerNibs()
    {
        let nib = UINib(nibName: "ServicesTableViewCell", bundle: nil)
        tableViewServices.register(nib, forCellReuseIdentifier: "ServicesTableViewCell")
        
        
        let nib1 = UINib(nibName: "QueuesTableViewCell", bundle: nil)
        listTableView.register(nib1, forCellReuseIdentifier: "QueuesTableViewCell")
        
    //    openingHoursTableView.register(UINib(nibName: "OpeningHoursTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let nib2 = UINib(nibName: "OpeningHoursTableViewCell", bundle: nil)
        listTableView.register(nib2, forCellReuseIdentifier: "OpeningHoursTableViewCell")
        
        
        optionsCollectionView.register(UINib(nibName: "serachVenderTabCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "serachVenderTabCollectionViewCell")

        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath:  IndexPath) -> UICollectionViewCell
    {
        
        let cell : serachVenderTabCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "serachVenderTabCollectionViewCell", for: indexPath as IndexPath) as! serachVenderTabCollectionViewCell
        
        cell.lblOptions.text = optionsArray[indexPath.row]
        
        if selectedIndexPath == indexPath.item {
        
            cell.sepratorView.isHidden = false
            cell.sepratorView.backgroundColor = colorWithHexString(hexString: "#57968B")
        }
        else
        {
            cell.sepratorView.isHidden = true

        }
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return optionsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        selectedIndexPath = indexPath.item
        
    optionsCollectionView.reloadData()
        
        if indexPath.item == 0
        {
            listTableView .reloadData()
            listTableView .isHidden = false
            tableViewServices .isHidden = true
            openingHoursTableView.isHidden = true
            googleMapView.isHidden = true

        }
        else if indexPath.item == 1
        {
            listTableView .isHidden = true
            tableViewServices .isHidden = false
            openingHoursTableView.isHidden = true
            googleMapView.isHidden = true
            
            
            get_service_Details()
            
            
        }
        else if indexPath.item == 2
        {
            listTableView.isHidden = true
            tableViewServices.isHidden = true
            openingHoursTableView.isHidden = false
            googleMapView.isHidden = true
            //openingHoursTableView.reloadData()
            
            get_OpeningHours_Details()
            
        }else
        {
            listTableView.isHidden = true
            tableViewServices.isHidden = true
            openingHoursTableView.isHidden = false
            
            googleMapView.isHidden = false
            
            
            showMarker()
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        if indexPath.item == 0
        {
            return CGSize(width:90 , height:60 )

        }
        else if indexPath.item == 1
        {
            return CGSize(width:105 , height:60 )
            
        }
        if indexPath.item == 2
        {
            return CGSize(width:145 , height:60 )
            
        }
       return CGSize(width:70 , height:60 )
    }
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }

}
