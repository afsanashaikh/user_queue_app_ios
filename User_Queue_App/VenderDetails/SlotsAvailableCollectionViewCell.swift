//
//  SlotsAvailableCollectionViewCell.swift
//  User_Queue_App
//
//  Created by Mac on 18/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SlotsAvailableCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var outerView: UIView!
    
    
    
    @IBOutlet weak var innerView: UIView!
    
    
    @IBOutlet weak var firstLbl: UILabel!
    
    @IBOutlet weak var secondLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        outerView.layer.cornerRadius = 10
        outerView.clipsToBounds = true
        outerView.layer.masksToBounds = true
        
        innerView.layer.cornerRadius = 10
        innerView.clipsToBounds = true
        innerView.layer.masksToBounds = true
    }
    
}
