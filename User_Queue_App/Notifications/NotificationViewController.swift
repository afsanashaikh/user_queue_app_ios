//
//  NotificationViewController.swift
//  User_Queue_App
//
//  Created by Mac on 17/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var lblNotifications: UILabel!
    
    @IBOutlet weak var lblNotificationList: UITableView!
    
    @IBOutlet weak var tableViewNotifications: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableViewNotifications.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NotificationTableViewCell
        
        
        if indexPath.row == 2 {
            cell1.lblStatus.text = "Declined"
            cell1.lblStatus.backgroundColor = colorWithHexString(hexString: "#FFEBEA")
            cell1.lblStatus.textColor = colorWithHexString(hexString: "#E96F76")
            
            cell1.reasonViewHeight.constant = 60
        }
        
        else
        {
            cell1.lblStatus.text = "Approved"
            cell1.lblStatus.backgroundColor = colorWithHexString(hexString: "#DEFDF7")
            cell1.lblStatus.textColor = colorWithHexString(hexString: "#57968B")
            
            cell1.reasonViewHeight.constant = 0
        }
        return cell1
    }
    func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
        
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    @IBAction func btnSerachClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "SearchVendorViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func btnNotificationClicked(_ sender: Any)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "NotificationViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
    @IBAction func btnHomeClicked(_ sender: Any)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVc=storyBoard .instantiateViewController(withIdentifier: "DashboardViewController")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(objVc, animated: true)
    }
    
}
